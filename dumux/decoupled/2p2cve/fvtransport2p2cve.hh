// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCModel
 * \brief Finite volume discretization of the component transport equation
 */
#ifndef DUMUX_FVTRANSPORT2P2CVE_HH
#define DUMUX_FVTRANSPORT2P2CVE_HH

#include <cmath>
#include <unordered_map>

#include <dune/grid/common/gridenums.hh>
#include <dune/common/float_cmp.hh>

#include <dumux/porousmediumflow/2p2c/sequential/fvtransport.hh>
#include <dumux/material/fluidstates/pseudo1p2c.hh>

namespace Dumux {
/*!
 * \ingroup SequentialTwoPTwoCModel
 * \brief Compositional transport step in a Finite Volume discretization.
 *
 *  The finite volume model for the solution of the transport equation for compositional
 *  two-phase flow.
 *  \f[
      \frac{\partial C^\kappa}{\partial t} = - \nabla \cdot \left( \sum_{\alpha} X^{\kappa}_{\alpha}
      \varrho_{alpha} \bf{v}_{\alpha}\right) + q^{\kappa},
 *  \f]
 *  where \f$ \bf{v}_{\alpha} = - \lambda_{\alpha} \bf{K} \left(\nabla p_{\alpha} + \rho_{\alpha} \bf{g} \right) \f$.
 *  \f$ p_{\alpha} \f$ denotes the phase pressure, \f$ \bf{K} \f$ the absolute permeability, \f$ \lambda_{\alpha} \f$ the phase mobility,
 *  \f$ \rho_{\alpha} \f$ the phase density and \f$ \bf{g} \f$ the gravity constant and \f$ C^{\kappa} \f$ the total Component concentration.
 *  The whole flux contribution for each cell is subdivided into a storage term, a flux term and a source term.
 *  Corresponding functions (<tt>getFlux()</tt> and <tt>getFluxOnBoundary()</tt>) are provided,
 *  internal sources are directly treated.
 *
 *  \tparam TypeTag The Type Tag
 */
template<class TypeTag>
class FVTransport2P2CVE : public FVTransport2P2C<TypeTag>
{
    typedef FVTransport2P2C<TypeTag> ParentType;

    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Implementation = GetPropType<TypeTag, Properties::TransportModel>;

    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using Indices = GetPropType<TypeTag, Properties::Indices>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using FluidState1p2c = PseudoOnePTwoCFluidState<Scalar, FluidSystem>;

    using CellData = GetPropType<TypeTag, Properties::CellData>;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };
    enum
    {
        pw = Indices::pressureW,
        pn = Indices::pressureN
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx, nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx, nCompIdx = Indices::nPhaseIdx,
        contiWEqIdx=Indices::contiWEqIdx, contiNEqIdx=Indices::contiNEqIdx,
        NumPhases = getPropValue<TypeTag, Properties::NumPhases>(),
        NumComponents = getPropValue<TypeTag, Properties::NumComponents>()
    };

    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };


    using Intersection = typename GridView::Intersection;

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using DimMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
    using PhaseVector = Dune::FieldVector<Scalar, NumPhases>;
    using ComponentVector = Dune::FieldVector<Scalar, NumComponents>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

public:
    //! @copydoc FVPressure::EntryType
    using EntryType = Dune::FieldVector<Scalar, 2>;

protected:
    //! Acess function for the current problem
    Problem& problem()
    { return problem_; }

public:
    // Function which calculates the flux update
    void getFlux(ComponentVector& fluxEntries, EntryType& timestepFlux,
                 const Intersection& intersection, CellData& cellDataI);

    // Function which calculates the boundary flux update
    void getFluxOnBoundary(ComponentVector& fluxEntries, EntryType& timestepFlux,
                           const Intersection& intersection, const CellData& cellDataI);

    //@}
    /*!
     * \brief Constructs a FVTransport2P2CVE object
     * Currently, the compositional transport scheme can not be applied with a global pressure / total velocity
     * formulation.
     *
     * \param problem a problem class object
     */
    FVTransport2P2CVE(Problem& problem) :
    ParentType(problem), problem_(problem), switchNormals(getParam<bool>("Impet.SwitchNormals"))
    {
        restrictFluxInTransport_ = getParam<int>("Impet.RestrictFluxInTransport", 0);
        regulateBoundaryPermeability_ = getPropValue<TypeTag, Properties::RegulateBoundaryPermeability>();
        if(regulateBoundaryPermeability_)
            minimalBoundaryPermeability_ = getParam<Scalar>("SpatialParams.MinBoundaryPermeability");

        Scalar cFLFactor = getParam<Scalar>("Impet.CFLFactor");

        lambda_ = getParam<Scalar>("SpatialParams.Lambda");
        entryP_ = getParam<Scalar>("SpatialParams.EntryPressure");
    }

    virtual ~FVTransport2P2CVE()
    {
    }

protected:
    Problem& problem_;
    int averagedFaces_; //!< number of faces were flux was restricted

    //! gives kind of pressure used (\f$ 0 = p_w \f$, \f$ 1 = p_n \f$, \f$ 2 = p_{global} \f$)
    static const int pressureType = getPropValue<TypeTag, Properties::PressureFormulation>();
    //! Restriction of flux on new pressure field if direction reverses from the pressure equation
    int restrictFluxInTransport_;
    //! Enables regulation of permeability in the direction of a Dirichlet Boundary Condition
    bool regulateBoundaryPermeability_;
    //! Minimal limit for the boundary permeability
    Scalar minimalBoundaryPermeability_;
    bool switchNormals;
    Scalar lambda_;
    Scalar entryP_;

private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! @copydoc IMPETProblem::asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }
};


/*!
 * \brief  Get flux at an interface between two cells
 * The flux through \f$ \gamma \f$  is calculated according to the underlying pressure field,
 * calculated by the pressure model.
 *  \f[ - A_{\gamma} \mathbf{n}^T_{\gamma} \mathbf{K}  \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha}
     \mathbf{d}_{ij}  \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
                \sum_{\kappa} X^{\kappa}_{\alpha} \f]
 * Here, \f$ \mathbf{d}_{ij} \f$ is the normalized vector connecting the cell centers, and \f$ \mathbf{n}_{\gamma} \f$
 * represents the normal of the face \f$ \gamma \f$. Due to the nature of the primay Variable, the (volume-)specific
 * total mass concentration, this represents a mass flux per cell volume.
 *
 * \param fluxEntries The flux entries, mass influx from cell \f$j\f$ to \f$i\f$.
 * \param timestepFlux flow velocities for timestep estimation
 * \param intersection The intersection
 * \param cellDataI The cell data for cell \f$i\f$
 */
template<class TypeTag>
void FVTransport2P2CVE<TypeTag>::getFlux(ComponentVector& fluxEntries,
                                        Dune::FieldVector<Scalar, 2>& timestepFlux,
                                        const Intersection& intersection,
                                        CellData& cellDataI)
{
    if (cellDataI.veModel() == sharpInterface || cellDataI.veModel() == capillaryFringe)
    {
        fluxEntries = 0.;
        timestepFlux = 0.;
        // cell information
        auto elementI = intersection.inside();
        int eIdxGlobalI = problem().variables().index(elementI);

        // get position
        const GlobalPosition globalPos = elementI.geometry().center();
        const GlobalPosition& gravity_ = problem().gravity();
        // cell volume, assume linear map here
        Scalar volume = elementI.geometry().volume();

        // get values of cell I
        Scalar pressI = problem().pressureModel().pressure(eIdxGlobalI);
        Scalar pcI = cellDataI.capillaryPressure();
        DimMatrix K_I(problem().spatialParams().intrinsicPermeability(elementI));

        PhaseVector SmobI(0.);
        using std::max;
        SmobI[wPhaseIdx] = max((cellDataI.saturation(wPhaseIdx)
        - problem().spatialParams().materialLawParams(elementI).swr())
        , 1e-2);
        SmobI[nPhaseIdx] = max((cellDataI.saturation(nPhaseIdx)
        - problem().spatialParams().materialLawParams(elementI).snr())
        , 1e-2);

        Scalar densityWIPlume (0.), densityWIBelowPlume (0.), densityNWI(0.);
        densityWIPlume = cellDataI.densityLiquidInPlume();
        densityWIBelowPlume = cellDataI.densityLiquidBelowPlume();
        densityNWI = cellDataI.density(nPhaseIdx);

        // face properties
        GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
        if (switchNormals)
            unitOuterNormal *= -1.0;
        Scalar faceArea = intersection.geometry().volume();

        //    // local interface index
        //    const int indexInInside = intersection.indexInInside();
        //    const int indexInOutside = intersection.indexInOutside();

        // create vector for timestep and for update
        Dune::FieldVector<Scalar, 2> factor (0.);
        Dune::FieldVector<Scalar, 2> updFactor (0.);

        PhaseVector potentialInPlume(0.);
        Scalar potentialLiquidBelowPlume;

        // access neighbor
        auto neighbor = intersection.outside();
        int eIdxGlobalJ = problem().variables().index(neighbor);
        CellData& cellDataJ = problem().variables().cellData(eIdxGlobalJ);

        // neighbor cell center in global coordinates
        const GlobalPosition& globalPosNeighbor = neighbor.geometry().center();

        // distance vector between barycenters
        GlobalPosition distVec = globalPosNeighbor - globalPos;
        // compute distance between cell centers
        Scalar dist = distVec.two_norm();

        GlobalPosition unitDistVec(distVec);
        unitDistVec /= dist;

        // phase densities in neighbor
        Scalar densityWJPlume (0.), densityWJBelowPlume (0.), densityNWJ(0.);
        densityWJPlume = cellDataJ.densityLiquidInPlume();
        densityWJBelowPlume = cellDataJ.densityLiquidBelowPlume();
        densityNWJ = cellDataJ.density(nPhaseIdx);

        // average phase densities with central weighting
        double densityWPlume_mean = (densityWIPlume + densityWJPlume) * 0.5;
        double densityWBelowPlume_mean = (densityWIBelowPlume + densityWJBelowPlume) * 0.5;
        double densityNW_mean = (densityNWI + densityNWJ) * 0.5;

        double pressJ = problem().pressureModel().pressure(eIdxGlobalJ);
        Scalar pcJ = cellDataJ.capillaryPressure();

        // compute mean permeability
        DimMatrix meanK_(0.);
        harmonicMeanMatrix(meanK_,
                           K_I,
                           problem().spatialParams().intrinsicPermeability(neighbor));
        Dune::FieldVector<Scalar,dim> K(0);
        meanK_.umv(unitDistVec,K);

        // for reconstruction of pressures to work, pressures need to be set in cell data
        switch (pressureType)
        {
            case pw:
            {
                cellDataI.setPressure(wPhaseIdx, pressI);
                cellDataI.setPressure(nPhaseIdx, pressI + pcI);
                cellDataJ.setPressure(wPhaseIdx, pressJ);
                cellDataJ.setPressure(nPhaseIdx, pressJ + pcJ);
                break;
            }
            case pn:
            {
                cellDataI.setPressure(nPhaseIdx, pressI);
                cellDataI.setPressure(wPhaseIdx, pressI - pcI);
                cellDataJ.setPressure(nPhaseIdx, pressJ);
                cellDataJ.setPressure(wPhaseIdx, pressJ - pcJ);
                break;
            }
        }

        const Scalar aquiferHeight = problem_.aquiferHeight();
        const Scalar pressureWInPlumeI = problem_.pressureModel().reconstPressure(aquiferHeight, wPhaseIdx, elementI);
        const Scalar pressureWInPlumeJ = problem_.pressureModel().reconstPressure(aquiferHeight, wPhaseIdx, neighbor);

        // determine potentials for upwind
        potentialInPlume[wPhaseIdx] = (pressureWInPlumeI - pressureWInPlumeJ) / (dist);
        potentialInPlume[nPhaseIdx] = (cellDataI.pressure(nPhaseIdx) - cellDataJ.pressure(nPhaseIdx)) / (dist);
        potentialLiquidBelowPlume = (cellDataI.pressure(wPhaseIdx) - cellDataJ.pressure(wPhaseIdx)) / (dist);

        // add gravity term
        potentialInPlume[nPhaseIdx] +=  (gravity_ * unitDistVec) * densityNW_mean;
        potentialInPlume[wPhaseIdx] +=  (gravity_ * unitDistVec) * densityWPlume_mean;
        potentialLiquidBelowPlume += (gravity_ * unitDistVec) * densityWBelowPlume_mean;

        potentialInPlume[wPhaseIdx] *= fabs(K * unitOuterNormal);
        potentialInPlume[nPhaseIdx] *= fabs(K * unitOuterNormal);
        potentialLiquidBelowPlume *= fabs(K * unitOuterNormal);

        // determine upwinding direction, perform upwinding if possible
        Dune::FieldVector<bool, NumPhases> doUpwindingInPlume(true);
        PhaseVector lambdaInPlume(0.);
        for(int phaseIdx = 0; phaseIdx < NumPhases; phaseIdx++) // determine upwind mobilities in plume
        {
            int contiEqIdx = 0;
            if(phaseIdx == wPhaseIdx)
                contiEqIdx = contiWEqIdx;
            else
                contiEqIdx = contiNEqIdx;

            if(!this->impet_ || restrictFluxInTransport_==0) // perform a strict uwpind scheme
            {
                if (potentialInPlume[phaseIdx] > 0.)
                {
                    lambdaInPlume[phaseIdx] = cellDataI.mobilityInPlume(phaseIdx);
                    cellDataI.setUpwindCell(intersection.indexInInside(), contiEqIdx, true);

                }
                else if(potentialInPlume[phaseIdx] < 0.)
                {
                    lambdaInPlume[phaseIdx] = cellDataJ.mobilityInPlume(phaseIdx);
                    cellDataI.setUpwindCell(intersection.indexInInside(), contiEqIdx, false);
                }
                else
                {
                    doUpwindingInPlume[phaseIdx] = false;
                    cellDataI.setUpwindCell(intersection.indexInInside(), contiEqIdx, false);
                    cellDataJ.setUpwindCell(intersection.indexInOutside(), contiEqIdx, false);
                }
            }
            else // Transport after PE with check on flow direction
            {
                bool wasUpwindCell = cellDataI.isUpwindCell(intersection.indexInInside(), contiEqIdx);

                if (potentialInPlume[phaseIdx] > 0. && wasUpwindCell)
                    lambdaInPlume[phaseIdx] = cellDataI.mobilityInPlume(phaseIdx);
                else if (potentialInPlume[phaseIdx] < 0. && !wasUpwindCell)
                    lambdaInPlume[phaseIdx] = cellDataJ.mobilityInPlume(phaseIdx);
                // potential direction does not coincide with that of P.E.
                else if(restrictFluxInTransport_ == 2)   // perform central averaging for all direction changes
                    doUpwindingInPlume[phaseIdx] = false;
                else    // i.e. restrictFluxInTransport == 1
                {
                    //check if harmonic weighting is necessary
                    if (potentialInPlume[phaseIdx] > 0. &&
                        (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>(cellDataJ.mobilityInPlume(phaseIdx), 0.0, 1.0e-30)
                        // check if outflow induce neglected (i.e. mob=0) phase flux
                        || (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())))
                        lambdaInPlume[phaseIdx] = cellDataI.mobilityInPlume(phaseIdx);
                    else if (potentialInPlume[phaseIdx] < 0. &&
                        (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>(cellDataI.mobilityInPlume(phaseIdx), 0.0, 1.0e-30)
                        // check if inflow induce neglected phase flux
                        || (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())))
                        lambdaInPlume[phaseIdx] = cellDataJ.mobilityInPlume(phaseIdx);
                    else
                        doUpwindingInPlume[phaseIdx] = false;
                }

            }

            // do not perform upwinding if so desired
            if(!doUpwindingInPlume[phaseIdx])
            {
                //a) no flux if there wont be any flux regardless how to average/upwind
                if(cellDataI.mobilityInPlume(phaseIdx)+cellDataJ.mobilityInPlume(phaseIdx)==0.)
                {
                    potentialInPlume[phaseIdx] = 0;
                    continue;
                }

                //b) perform harmonic averaging
                if(phaseIdx == wPhaseIdx)
                {
                    fluxEntries[wCompIdx] -= potentialInPlume[phaseIdx] * faceArea / volume
                    * harmonicMean(cellDataI.massFractionLiquidInPlume(wCompIdx) * cellDataI.mobilityInPlume(phaseIdx) * cellDataI.densityLiquidInPlume(),
                                   cellDataJ.massFractionLiquidInPlume(wCompIdx) * cellDataJ.mobilityInPlume(phaseIdx) * cellDataJ.densityLiquidInPlume());
                    fluxEntries[nCompIdx] -= potentialInPlume[phaseIdx] * faceArea / volume
                    * harmonicMean(cellDataI.massFractionLiquidInPlume(nCompIdx) * cellDataI.mobilityInPlume(phaseIdx) * cellDataI.densityLiquidInPlume(),
                                   cellDataJ.massFractionLiquidInPlume(nCompIdx) * cellDataJ.mobilityInPlume(phaseIdx) * cellDataJ.densityLiquidInPlume());
                }
                else
                {
                    fluxEntries[wCompIdx] -= potentialInPlume[phaseIdx] * faceArea / volume
                    * harmonicMean(cellDataI.massFraction(phaseIdx, wCompIdx) * cellDataI.mobilityInPlume(phaseIdx) * cellDataI.density(phaseIdx),
                                   cellDataJ.massFraction(phaseIdx, wCompIdx) * cellDataJ.mobilityInPlume(phaseIdx) * cellDataJ.density(phaseIdx));
                    fluxEntries[nCompIdx] -= potentialInPlume[phaseIdx] * faceArea / volume
                    * harmonicMean(cellDataI.massFraction(phaseIdx, nCompIdx) * cellDataI.mobilityInPlume(phaseIdx) * cellDataI.density(phaseIdx),
                                   cellDataJ.massFraction(phaseIdx, nCompIdx) * cellDataJ.mobilityInPlume(phaseIdx) * cellDataJ.density(phaseIdx));
                }
                // c) timestep control
                // for timestep control : influx
                using std::max;
                timestepFlux[0] += max(0.,
                                       - potentialInPlume[phaseIdx] * faceArea / volume
                                       * harmonicMean(cellDataI.mobilityInPlume(phaseIdx),cellDataJ.mobilityInPlume(phaseIdx)));
                // outflux
                timestepFlux[1] += max(0.,
                                       potentialInPlume[phaseIdx] * faceArea / volume
                                       * harmonicMean(cellDataI.mobilityInPlume(phaseIdx),cellDataJ.mobilityInPlume(phaseIdx))/SmobI[phaseIdx]);

                //d) output
                if(!(cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())
                    && eIdxGlobalI > eIdxGlobalJ) //(only for one side)
                    {
                        averagedFaces_++;
                        #if DUNE_MINIMAL_DEBUG_LEVEL < 3
                        // verbose (only for one side)
                        if(eIdxGlobalI > eIdxGlobalJ)
                            Dune::dinfo << "harmonicMean flux of phase" << phaseIdx <<" used from cell" << eIdxGlobalI<< " into " << eIdxGlobalJ
                            << " ; TE upwind I = "<< cellDataI.isUpwindCell(intersection.indexInInside(), contiEqIdx)
                            << " but pot = "<< potentialInPlume[phaseIdx] <<  std::endl;
                        #endif
                    }

                    //e) stop further standard calculations
                    potentialInPlume[phaseIdx] = 0;
            }
        }

        //determine upwind mobility for liquid phase below plume
        bool doUpwindingLiquidBelowPlume(true);
        Scalar lambdaLiquidBelowPlume(0.);
        if(!this->impet_ || restrictFluxInTransport_==0) // perform a strict uwpind scheme
        {
            if (potentialLiquidBelowPlume > 0.)
            {
                lambdaLiquidBelowPlume = cellDataI.mobilityLiquidBelowPlume();
                cellDataI.setUpwindCellLiquidBelowPlume(intersection.indexInInside(), true);

            }
            else if(potentialLiquidBelowPlume < 0.)
            {
                lambdaLiquidBelowPlume = cellDataJ.mobilityLiquidBelowPlume();
                cellDataI.setUpwindCellLiquidBelowPlume(intersection.indexInInside(), false);
            }
            else
            {
                doUpwindingLiquidBelowPlume = false;
                cellDataI.setUpwindCellLiquidBelowPlume(intersection.indexInInside(), false);
                cellDataJ.setUpwindCellLiquidBelowPlume(intersection.indexInOutside(), false);
            }
        }
        else // Transport after PE with check on flow direction
        {
            bool wasUpwindCell = cellDataI.isUpwindCellLiquidBelowPlume(intersection.indexInInside());

            if (potentialLiquidBelowPlume > 0. && wasUpwindCell)
                lambdaLiquidBelowPlume = cellDataI.mobilityLiquidBelowPlume();
            else if (potentialLiquidBelowPlume < 0. && !wasUpwindCell)
                lambdaLiquidBelowPlume = cellDataJ.mobilityLiquidBelowPlume();
            // potential direction does not coincide with that of P.E.
            else if(restrictFluxInTransport_ == 2)   // perform central averaging for all direction changes
                doUpwindingLiquidBelowPlume = false;
            else    // i.e. restrictFluxInTransport == 1
            {
                //check if harmonic weighting is necessary
                if (potentialLiquidBelowPlume > 0. && (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>(cellDataJ.mobilityLiquidBelowPlume(), 0.0, 1.0e-30)   // check if outflow induce neglected (i.e. mob=0) phase flux
                    || (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())))
                    lambdaLiquidBelowPlume = cellDataI.mobilityLiquidBelowPlume();
                else if (potentialLiquidBelowPlume < 0. && (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>(cellDataI.mobilityLiquidBelowPlume(), 0.0, 1.0e-30) // check if inflow induce neglected phase flux
                    || (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())))
                    lambdaLiquidBelowPlume = cellDataJ.mobilityLiquidBelowPlume();
                else
                    doUpwindingLiquidBelowPlume = false;
            }

        }

        // do not perform upwinding if so desired
        if(!doUpwindingLiquidBelowPlume)
        {
            //a) no flux if there wont be any flux regardless how to average/upwind
            if(cellDataI.mobilityLiquidBelowPlume()+cellDataJ.mobilityLiquidBelowPlume()==0.)
            {
                potentialLiquidBelowPlume = 0;
            }
            else
            {
                //b) perform harmonic averaging
                fluxEntries[wCompIdx] -= potentialLiquidBelowPlume * faceArea / volume
                * harmonicMean(cellDataI.massFractionLiquidBelowPlume(wCompIdx) * cellDataI.mobilityLiquidBelowPlume() * cellDataI.densityLiquidBelowPlume(),
                               cellDataJ.massFractionLiquidBelowPlume(wCompIdx) * cellDataJ.mobilityLiquidBelowPlume() * cellDataJ.densityLiquidBelowPlume());
                fluxEntries[nCompIdx] -= potentialLiquidBelowPlume * faceArea / volume
                * harmonicMean(cellDataI.massFractionLiquidBelowPlume(nCompIdx) * cellDataI.mobilityLiquidBelowPlume() * cellDataI.densityLiquidBelowPlume(),
                               cellDataJ.massFractionLiquidBelowPlume(nCompIdx) * cellDataJ.mobilityLiquidBelowPlume() * cellDataJ.densityLiquidBelowPlume());
                // c) timestep control
                // for timestep control : influx
                using std::max;
                timestepFlux[0] += max(0.,
                                       - potentialLiquidBelowPlume * faceArea / volume
                                       * harmonicMean(cellDataI.mobilityLiquidBelowPlume(),cellDataJ.mobilityLiquidBelowPlume()));
                // outflux
                timestepFlux[1] += max(0.,
                                       potentialLiquidBelowPlume * faceArea / volume
                                       * harmonicMean(cellDataI.mobilityLiquidBelowPlume(),cellDataJ.mobilityLiquidBelowPlume())/SmobI[wPhaseIdx]);

                //d) output
                if(!(cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())
                    && eIdxGlobalI > eIdxGlobalJ) //(only for one side)
                    {
                        averagedFaces_++;
                        #if DUNE_MINIMAL_DEBUG_LEVEL < 3
                        // verbose (only for one side)
                        if(eIdxGlobalI > eIdxGlobalJ)
                            Dune::dinfo << "harmonicMean flux of phase" << phaseIdx <<" used from cell" << eIdxGlobalI<< " into " << eIdxGlobalJ
                            << " ; TE upwind I = "<< cellDataI.isUpwindCellLiquidBelowPlume(intersection.indexInInside())
                            << " but pot = "<< potentialLiquidBelowPlume <<  std::endl;
                        #endif
                    }

                    //e) stop further standard calculations
                    potentialLiquidBelowPlume = 0;
            }
        }

        // calculate and standardized velocity
        using std::max;
        double velocityJIwInPlume = max(-(lambdaInPlume[wPhaseIdx] * potentialInPlume[wPhaseIdx]) * faceArea / volume, 0.0);
        double velocityIJwInPlume = max(( lambdaInPlume[wPhaseIdx] * potentialInPlume[wPhaseIdx]) * faceArea / volume, 0.0);
        double velocityJIwBelowPlume = max(-(lambdaLiquidBelowPlume * potentialLiquidBelowPlume) * faceArea / volume, 0.0);
        double velocityIJwBelowPlume = max(( lambdaLiquidBelowPlume * potentialLiquidBelowPlume) * faceArea / volume, 0.0);
        double velocityJIn = max(-(lambdaInPlume[nPhaseIdx] * potentialInPlume[nPhaseIdx]) * faceArea / volume, 0.0);
        double velocityIJn = max(( lambdaInPlume[nPhaseIdx] * potentialInPlume[nPhaseIdx]) * faceArea / volume, 0.0);

        // for timestep control : influx
        timestepFlux[0] += velocityJIwInPlume + velocityJIwBelowPlume + velocityJIn;

        double foutw = (velocityIJwInPlume + velocityIJwBelowPlume)/SmobI[wPhaseIdx];
        double foutn = velocityIJn/SmobI[nPhaseIdx];
        using std::isnan;
        using std::isinf;
        if (isnan(foutw) || isinf(foutw) || foutw < 0) foutw = 0;
        if (isnan(foutn) || isinf(foutn) || foutn < 0) foutn = 0;
        timestepFlux[1] += foutw + foutn;

        fluxEntries[wCompIdx] +=
        velocityJIwInPlume * cellDataJ.massFractionLiquidInPlume(wCompIdx) * densityWJPlume
        - velocityIJwInPlume * cellDataI.massFractionLiquidInPlume(wCompIdx) * densityWIPlume
        + velocityJIwBelowPlume * cellDataJ.massFractionLiquidBelowPlume(wCompIdx) * densityWJBelowPlume
        - velocityIJwBelowPlume * cellDataI.massFractionLiquidBelowPlume(wCompIdx) * densityWIBelowPlume
        + velocityJIn * cellDataJ.massFraction(nPhaseIdx, wCompIdx) * densityNWJ
        - velocityIJn * cellDataI.massFraction(nPhaseIdx, wCompIdx) * densityNWI;
        fluxEntries[nCompIdx] +=
        velocityJIwInPlume * cellDataJ.massFractionLiquidInPlume(nCompIdx) * densityWJPlume
        - velocityIJwInPlume * cellDataI.massFractionLiquidInPlume(nCompIdx) * densityWIPlume
        + velocityJIwBelowPlume * cellDataJ.massFractionLiquidBelowPlume(nCompIdx) * densityWJBelowPlume
        - velocityIJwBelowPlume * cellDataI.massFractionLiquidBelowPlume(nCompIdx) * densityWIBelowPlume
        + velocityJIn * cellDataJ.massFraction(nPhaseIdx, nCompIdx) * densityNWJ
        - velocityIJn * cellDataI.massFraction(nPhaseIdx, nCompIdx) * densityNWI;
    }
    else
    {
        ParentType::getFlux(fluxEntries, timestepFlux, intersection, cellDataI);
    }
    return;
}

/*!
 * \brief Get flux on Boundary
 *
 * The flux through \f$ \gamma \f$  is calculated according to the underlying pressure field,
 * calculated by the pressure model.
 *  \f[ - A_{\gamma}  \mathbf{n}^T_{\gamma} \mathbf{K} \mathbf{d}_{i-Boundary}
      \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha} \sum_{\kappa} X^{\kappa}_{\alpha}
    \left( \frac{p_{\alpha,Boundary}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha}\mathbf{g}^T \mathbf{d}_{i-Boundary}\right) \f]
 * Here, \f$ \mathbf{u} \f$ is the normalized vector connecting the cell centers, and \f$ \mathbf{n}_{\gamma_{ij}} \f$
 * represents the normal of the face \f$ \gamma{ij} \f$.
 * \param fluxEntries The flux entries, mass influx from cell \f$j\f$ to \f$i\f$.
 * \param timestepFlux flow velocities for timestep estimation
 * \param intersection The intersection
 * \param cellDataI The cell data for cell \f$i\f$
 */
template<class TypeTag>
void FVTransport2P2CVE<TypeTag>::getFluxOnBoundary(ComponentVector& fluxEntries,
                                                 Dune::FieldVector<Scalar, 2>& timestepFlux,
                                                 const Intersection& intersection,
                                                 const CellData& cellDataI)
{
    int veModel = cellDataI.veModel();
    if (veModel == sharpInterface || veModel == capillaryFringe)
    {
        using std::max;
        // cell information
        auto elementI = intersection.inside();
        int eIdxGlobalI = problem().variables().index(elementI);

        // get position
        const GlobalPosition globalPos = elementI.geometry().center();

        // cell volume, assume linear map here
        Scalar volume = elementI.geometry().volume();
        const GlobalPosition& gravity_ = problem().gravity();
        // get values of cell I
        Scalar pressI = problem().pressureModel().pressure(eIdxGlobalI);
        Scalar pcI = cellDataI.capillaryPressure();
        DimMatrix K_I(problem().spatialParams().intrinsicPermeability(elementI));

        if(regulateBoundaryPermeability_)
        {
            int axis = intersection.indexInInside() / 2;
            if(K_I[axis][axis] < minimalBoundaryPermeability_)
                K_I[axis][axis] = minimalBoundaryPermeability_;
        }

        Scalar SwmobI = max((cellDataI.saturation(wPhaseIdx)
        - problem().spatialParams().materialLawParams(elementI).swr())
        , 1e-2);
        Scalar SnmobI = max((cellDataI.saturation(nPhaseIdx)
        - problem().spatialParams().materialLawParams(elementI).snr())
        , 1e-2);

        Scalar densityWI (0.), densityNWI(0.);
        densityWI= cellDataI.density(wPhaseIdx);
        densityNWI = cellDataI.density(nPhaseIdx);

        // face properties
        GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
        if (switchNormals)
            unitOuterNormal *= -1.0;
        Scalar faceArea = intersection.geometry().volume();

        // create vector for timestep and for update
        Dune::FieldVector<Scalar, 2> factor (0.);
        Dune::FieldVector<Scalar, 2> updFactor (0.);

        PhaseVector potential(0.);
        // center of face in global coordinates
        const GlobalPosition& globalPosFace = intersection.geometry().center();

        // distance vector between barycenters
        GlobalPosition distVec = globalPosFace - globalPos;
        // compute distance between cell centers
        Scalar dist = distVec.two_norm();

        GlobalPosition unitDistVec(distVec);
        unitDistVec /= dist;

        //instantiate a fluid state
        FluidState BCfluidState;

        //get boundary type
        BoundaryTypes bcTypes;
        problem().boundaryTypes(bcTypes, intersection);

        /**********         Dirichlet Boundary        *************/
        if (bcTypes.isDirichlet(contiWEqIdx)) // if contiWEq is Dirichlet, so is contiNEq
        {
            //read boundary values
            PrimaryVariables primaryVariablesOnBoundary(NAN);
            problem().dirichlet(primaryVariablesOnBoundary, intersection);

            // prepare pressure boundary condition
            PhaseVector pressBC(0.);
            Scalar pressBound = primaryVariablesOnBoundary[Indices::pressureEqIdx];
            Scalar pcBound (0.);

            //pressure at boundary is defined at the cell center, but needed at bottom for ve models
            Scalar pRef = problem_.referencePressureAtPos(globalPos);
            Scalar tempRef = problem_.temperatureAtPos(globalPos);
            FluidState BCfluidState;
            BCfluidState.setPressure(wPhaseIdx, pRef);
            BCfluidState.setPressure(nPhaseIdx, pRef);
            BCfluidState.setTemperature(tempRef);
            BCfluidState.setMassFraction(wPhaseIdx, wCompIdx, 1.);
            BCfluidState.setMassFraction(nPhaseIdx, wCompIdx, 0.);
            Scalar gravity = problem_.gravity().two_norm();
            switch (pressureType)
            {
                case pw:
                {
                    pressBound = pressBound + (FluidSystem::density(BCfluidState, wPhaseIdx) * gravity * problem_.aquiferHeight()/2.0);
                    break;
                }
                case pn:
                {
                    pressBound = pressBound + (FluidSystem::density(BCfluidState, nPhaseIdx) * gravity * problem_.aquiferHeight()/2.0);
                    break;
                }
            }

            //assume no gas on boundary and neglect difference above/below plume for cell inside TODO: extend
            //iterate capillary pressure and gasPlumeDist
            unsigned int maxiter = 6;
            pcBound = cellDataI.pressure(nPhaseIdx) - cellDataI.pressure(wPhaseIdx); // initial guess for pc from cell inside
            PhaseVector referencePressurePlume;

            Scalar densityWInPlume = 0.0;
            Scalar densityNInPlume = 0.0;
            Scalar densityWBelowPlume = 0.0;
            Scalar viscosityWInPlume = 0.0;
            //start iteration loop
            for (unsigned int iter = 0; iter < maxiter; iter++)
            {
                switch (pressureType)
                {
                    case pw:
                    {
                        pressBC[wPhaseIdx] = pressBound;
                        pressBC[nPhaseIdx] = pressBound + pcBound;
                        break;
                    }
                    case pn:
                    {
                        pressBC[wPhaseIdx] = pressBound - pcBound;
                        pressBC[nPhaseIdx] = pressBound;
                        break;
                    }
                }

                //calculate new pc
                Scalar oldPcBound = pcBound;

                if(iter == 0)
                    referencePressurePlume = pressBC;
                const FluidState fluidStatePlume = problem().pressureModel().fluidStatePlume(elementI, referencePressurePlume);
                const FluidState1p2c fluidStateBelowPlume = problem().pressureModel().fluidStateBelowPlume(elementI, pressBC);

                Scalar gasPlumeDist = problem_.aquiferHeight();
                Scalar minGasPlumeDist = problem_.aquiferHeight();

                densityWBelowPlume = fluidStateBelowPlume.density(wPhaseIdx);
                densityWInPlume = fluidStatePlume.density(wPhaseIdx);
                densityNInPlume = fluidStatePlume.density(nPhaseIdx);
                viscosityWInPlume = fluidStateBelowPlume.viscosity(wPhaseIdx);

                if (veModel == sharpInterface)
                {
                    pcBound = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                    - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist);
                }
                else if (veModel == capillaryFringe)
                {
                    pcBound = densityNInPlume * gravity * gasPlumeDist - densityWBelowPlume * gravity * minGasPlumeDist
                    - densityWInPlume * gravity * (gasPlumeDist - minGasPlumeDist) + entryP_;
                }

                referencePressurePlume[wPhaseIdx] = pressBC[wPhaseIdx] - densityWBelowPlume * gravity * minGasPlumeDist;
                referencePressurePlume[nPhaseIdx] = pressBC[wPhaseIdx] + pcBound
                - densityNInPlume * gravity * problem_.aquiferHeight();

                if ((fabs(oldPcBound-pcBound)<10 || !getPropValue<TypeTag, Properties::EnableCapillarity>()) && iter != 0)
                    break;
            }

            // determine fluid properties at the boundary
            Scalar densityWBound = densityWBelowPlume;
            Scalar densityNWBound = densityNInPlume;

            // average
            double densityW_mean = (densityWI + densityWBound) / 2;
            double densityNW_mean = (densityNWI + densityNWBound) / 2;

            // prepare K
            Dune::FieldVector<Scalar,dim> K(0);
            K_I.umv(unitDistVec,K);

            //calculate potential gradient
            switch (pressureType)
            {
                case pw:
                {
                    potential[wPhaseIdx] = (pressI - pressBC[wPhaseIdx]) / dist;
                    potential[nPhaseIdx] = (pressI + pcI - pressBC[wPhaseIdx] - pcBound) / dist;
                    break;
                }
                case pn:
                {
                    potential[wPhaseIdx] = (pressI - pcI - pressBC[nPhaseIdx] + pcBound) / dist;
                    potential[nPhaseIdx] = (pressI - pressBC[nPhaseIdx]) / dist;
                    break;
                }
            }
            potential[wPhaseIdx] += (gravity_ * unitDistVec) * densityW_mean;
            potential[nPhaseIdx] += (gravity_ * unitDistVec) * densityNW_mean;

            potential[wPhaseIdx] *= fabs(K * unitOuterNormal);
            potential[nPhaseIdx] *= fabs(K * unitOuterNormal);

            // do upwinding for lambdas
            PhaseVector lambda(0.);
            if (potential[wPhaseIdx] >= 0.)
                lambda[wPhaseIdx] = cellDataI.mobility(wPhaseIdx);
            else
            {
                lambda[wPhaseIdx] = 1./viscosityWInPlume;
            }
            if (potential[nPhaseIdx] >= 0.)
                lambda[nPhaseIdx] = cellDataI.mobility(nPhaseIdx);
            else
            {
                lambda[nPhaseIdx] = 0.0;
            }

            // calculate and standardized velocity
            double velocityJIw = max((-lambda[wPhaseIdx] * potential[wPhaseIdx]) * faceArea / volume, 0.0);
            double velocityIJw = max(( lambda[wPhaseIdx] * potential[wPhaseIdx]) * faceArea / volume, 0.0);
            double velocityJIn = max((-lambda[nPhaseIdx] * potential[nPhaseIdx]) * faceArea / volume, 0.0);
            double velocityIJn = max(( lambda[nPhaseIdx] * potential[nPhaseIdx]) * faceArea / volume, 0.0);

            // for timestep control
            timestepFlux[0] = velocityJIw + velocityJIn;

            double foutw = velocityIJw/SwmobI;
            double foutn = velocityIJn/SnmobI;
            using std::isnan;
            using std::isinf;
            if (isnan(foutw) || isinf(foutw) || foutw < 0) foutw = 0;
            if (isnan(foutn) || isinf(foutn) || foutn < 0) foutn = 0;
            timestepFlux[1] = foutw + foutn;

            fluxEntries[wCompIdx] =
            + velocityJIw * 1.0 /*=BCfluidState.massFraction(wPhaseIdx, wCompIdx)*/ * densityWBound
            - velocityIJw * cellDataI.massFraction(wPhaseIdx, wCompIdx) * densityWI
            + velocityJIn * 0.0 /*=BCfluidState.massFraction(nPhaseIdx, wCompIdx)*/ * densityNWBound
            - velocityIJn * cellDataI.massFraction(nPhaseIdx, wCompIdx) * densityNWI ;
            fluxEntries[nCompIdx] =
            velocityJIw * 0.0 /*=BCfluidState.massFraction(wPhaseIdx, nCompIdx)*/ * densityWBound
            - velocityIJw * cellDataI.massFraction(wPhaseIdx, nCompIdx) * densityWI
            + velocityJIn * 1.0 /*=BCfluidState.massFraction(nPhaseIdx, nCompIdx)*/ * densityNWBound
            - velocityIJn * cellDataI.massFraction(nPhaseIdx, nCompIdx) * densityNWI ;
        }//end dirichlet boundary
        else if (bcTypes.isNeumann(contiWEqIdx))
        {
            // Convention: outflow => positive sign : has to be subtracted from update vec
            PrimaryVariables J(NAN);
            problem().neumann(J, intersection);
            fluxEntries[wCompIdx] = - J[contiWEqIdx] * faceArea / volume;
            fluxEntries[nCompIdx] = - J[contiNEqIdx] * faceArea / volume;

            // for timestep control
            #define cflIgnoresNeumann
            #ifdef cflIgnoresNeumann
            timestepFlux[0] = 0;
            timestepFlux[1] = 0;
            #else
            double inflow = updFactor[wCompIdx] / densityW + updFactor[nCompIdx] / densityNW;
            if (inflow>0)
            {
                timestepFlux[0] = updFactor[wCompIdx] / densityW
                + updFactor[nCompIdx] / densityNW;    // =factor in
                timestepFlux[1] = -(updFactor[wCompIdx] / densityW /SwmobI
                + updFactor[nCompIdx] / densityNW / SnmobI);    // =factor out
            }
            else
            {
                timestepFlux[0] = -(updFactor[wCompIdx] / densityW
                + updFactor[nCompIdx] / densityNW);    // =factor in
                timestepFlux[1] = updFactor[wCompIdx] / densityW /SwmobI
                + updFactor[nCompIdx] / densityNW / SnmobI;    // =factor out
            }
            #endif
        }//end neumann boundary
    }
    else
    {
        ParentType::getFluxOnBoundary(fluxEntries, timestepFlux, intersection, cellDataI);
    }
    return;
}

} // end namespace Dumux
#endif
