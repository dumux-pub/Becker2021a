// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCModel
 * \brief Finite volume discretization of the component transport equation
 */
#ifndef DUMUX_FVTRANSPORT2P2CVEMULTIDIM_HH
#define DUMUX_FVTRANSPORT2P2CVEMULTIDIM_HH

#include <cmath>
#include <unordered_map>

#include <dune/grid/common/gridenums.hh>
#include <dune/common/float_cmp.hh>

#include "fvtransport2p2cve.hh"
#include <dumux/material/fluidstates/pseudo1p2c.hh>

namespace Dumux {
/*!
 * \ingroup SequentialTwoPTwoCModel
 * \brief Compositional transport step in a Finite Volume discretization.
 *
 *  The finite volume model for the solution of the transport equation for compositional
 *  two-phase flow.
 *  \f[
      \frac{\partial C^\kappa}{\partial t} = - \nabla \cdot \left( \sum_{\alpha} X^{\kappa}_{\alpha}
      \varrho_{alpha} \bf{v}_{\alpha}\right) + q^{\kappa},
 *  \f]
 *  where \f$ \bf{v}_{\alpha} = - \lambda_{\alpha} \bf{K} \left(\nabla p_{\alpha} + \rho_{\alpha} \bf{g} \right) \f$.
 *  \f$ p_{\alpha} \f$ denotes the phase pressure, \f$ \bf{K} \f$ the absolute permeability, \f$ \lambda_{\alpha} \f$ the phase mobility,
 *  \f$ \rho_{\alpha} \f$ the phase density and \f$ \bf{g} \f$ the gravity constant and \f$ C^{\kappa} \f$ the total Component concentration.
 *  The whole flux contribution for each cell is subdivided into a storage term, a flux term and a source term.
 *  Corresponding functions (<tt>getFlux()</tt> and <tt>getFluxOnBoundary()</tt>) are provided,
 *  internal sources are directly treated.
 *
 *  \tparam TypeTag The Type Tag
 */
template<class TypeTag>
class FVTransport2P2CVEMultiDim : public FVTransport2P2CVE<TypeTag>
{
    typedef FVTransport2P2CVE<TypeTag> ParentType;

    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Implementation = GetPropType<TypeTag, Properties::TransportModel>;

    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using Indices = GetPropType<TypeTag, Properties::Indices>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using FluidState1p2c = PseudoOnePTwoCFluidState<Scalar, FluidSystem>;

    using CellData = GetPropType<TypeTag, Properties::CellData>;

    using TransportSolutionType = GetPropType<TypeTag, Properties::TransportSolutionType>;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };
    enum
    {
        pw = Indices::pressureW,
        pn = Indices::pressureN
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx, nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx, nCompIdx = Indices::nPhaseIdx,
        contiWEqIdx=Indices::contiWEqIdx, contiNEqIdx=Indices::contiNEqIdx,
        NumPhases = getPropValue<TypeTag, Properties::NumPhases>(),
        numComponents = getPropValue<TypeTag, Properties::NumComponents>()
    };

    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };


    using Intersection = typename GridView::Intersection;

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using DimMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
    using PhaseVector = Dune::FieldVector<Scalar, NumPhases>;
    using ComponentVector = Dune::FieldVector<Scalar, numComponents>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using CellArray = std::array<unsigned int, dimWorld>;

    using LocalTimesteppingData = typename FVTransport2P2C<TypeTag>::LocalTimesteppingData;

public:
    //! @copydoc FVPressure::EntryType
    using EntryType = Dune::FieldVector<Scalar, 2>;

protected:
    //! Acess function for the current problem
    Problem& problem()
    { return problem_; }

public:
    virtual void update(const Scalar t, Scalar& dt, TransportSolutionType& updateVec, bool impes = false);

    // Function which calculates the flux update
    void getFlux(ComponentVector& fluxEntries, EntryType& timestepFlux,
                 const Intersection& intersection, CellData& cellDataI);

    //@}
    /*!
     * \brief Constructs a FVTransport2P2CVE object
     * Currently, the compositional transport scheme can not be applied with a global pressure / total velocity
     * formulation.
     *
     * \param problem a problem class object
     */
    FVTransport2P2CVEMultiDim(Problem& problem) :
    ParentType(problem), problem_(problem), switchNormals(getParam<bool>("Impet.SwitchNormals"))
    {
        restrictFluxInTransport_ = getParam<int>("Impet.RestrictFluxInTransport", 0);
        regulateBoundaryPermeability_ = getPropValue<TypeTag, Properties::RegulateBoundaryPermeability>();
        if(regulateBoundaryPermeability_)
            minimalBoundaryPermeability_ = getParam<Scalar>("SpatialParams.MinBoundaryPermeability");

        Scalar cFLFactor = getParam<Scalar>("Impet.CFLFactor");

        lambda_ = getParam<Scalar>("SpatialParams.Lambda");
        entryP_ = getParam<Scalar>("SpatialParams.EntryPressure");
    }

    virtual ~FVTransport2P2CVEMultiDim()
    {
    }

protected:
    Problem& problem_;
    int averagedFaces_; //!< number of faces were flux was restricted

    //! gives kind of pressure used (\f$ 0 = p_w \f$, \f$ 1 = p_n \f$, \f$ 2 = p_{global} \f$)
    static const int pressureType = getPropValue<TypeTag, Properties::PressureFormulation>();
    //! Restriction of flux on new pressure field if direction reverses from the pressure equation
    int restrictFluxInTransport_;
    //! Enables regulation of permeability in the direction of a Dirichlet Boundary Condition
    bool regulateBoundaryPermeability_;
    //! Minimal limit for the boundary permeability
    Scalar minimalBoundaryPermeability_;
    bool switchNormals;
    Scalar lambda_;
    Scalar entryP_;

private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! @copydoc IMPETProblem::asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }
};

/*!
 *  \brief Calculate the update vector and determine timestep size
 *  This method calculates the update vector \f$ u \f$ of the discretized equation
 *  \f[
 *      C^{\kappa , new} = C^{\kappa , old} + u,
 *  \f]
 *  where \f$ u = \sum_{\gamma} \boldsymbol{v}_{\alpha} * \varrho_{\alpha} * X^{\kappa}_{\alpha} * \boldsymbol{n} * A_{\gamma} \f$,
 *  \f$ \boldsymbol{n} \f$ is the face normal and \f$ A_{\gamma} \f$ is the face area of face \f$ \gamma \f$.
 *
 *  In addition to the \a update vector, the recommended time step size \a dt is calculated
 *  employing a CFL condition.
 *
 *  \param t Current simulation time \f$\mathrm{[s]}\f$
 *  \param dt Time step size \f$\mathrm{[s]}\f$
 *  \param updateVec Update vector, or update estimate for secants, resp. Here in \f$\mathrm{[kg/m^3]}\f$
 *  \param impet Flag that determines if it is a real impet step or an update estimate for volume derivatives
 */
template<class TypeTag>
void FVTransport2P2CVEMultiDim<TypeTag>::update(const Scalar t, Scalar& dt,
                                      TransportSolutionType& updateVec, bool impet)
{
    // initialize dt very large
    dt = 1E100;

    // resize update vector and set to zero
    unsigned int size = problem_.gridView().size(0);
    updateVec.resize(getPropValue<TypeTag, Properties::NumComponents>());
    updateVec[wCompIdx].resize(size);
    updateVec[nCompIdx].resize(size);
    updateVec[wCompIdx] = 0;
    updateVec[nCompIdx] = 0;

    if (this->localTimeStepping_)
    {
        if (this->timeStepData_.size() != size)
            this->timeStepData_.resize(size);
    }
    // store if we do update Estimate for flux functions
    this->impet_ = impet;
    averagedFaces_ = 0;

    //also resize PV vector if necessary
    if(this->totalConcentration_.size() != size)
    {
        this->totalConcentration_[wCompIdx].resize(size);
        this->totalConcentration_[nCompIdx].resize(size);
        // copy data //TODO: remove this, remove PM Transport Vector!!
        // loop through all elements
        for (int i = 0; i< problem().gridView().size(0); i++)
        {
            CellData& cellDataI = problem().variables().cellData(i);
            for(int compIdx = 0; compIdx < getPropValue<TypeTag, Properties::NumComponents>(); compIdx++)
            {
                this->totalConcentration_[compIdx][i]
                = cellDataI.totalConcentration(compIdx);
            }
        }
    }

    // Cell which restricts time step size
    int restrictingCell = -1;

    ComponentVector entries(0.);
    EntryType timestepFlux(0.);
    // compute update vector
    for (const auto& element : elements(problem().gridView()))
    {
        // get cell infos
        int eIdxGlobalI = problem().variables().index(element);
        CellData& cellDataI = problem().variables().cellData(eIdxGlobalI);

        // some variables for time step calculation
        double sumfactorin = 0;
        double sumfactorout = 0;

        // run through all intersections with neighbors and boundary
        for (const auto& intersection : intersections(problem().gridView(), element))
        {
            int indexInInside = intersection.indexInInside();

            /****** interior face   *****************/
            if (intersection.neighbor())
                asImp_().getFlux(entries, timestepFlux, intersection, cellDataI);

            /******  Boundary Face   *****************/
            if (intersection.boundary())
                asImp_().getFluxOnBoundary(entries, timestepFlux, intersection, cellDataI);


            if (this->localTimeStepping_)
            {
                LocalTimesteppingData& localData = this->timeStepData_[eIdxGlobalI];
                if (localData.faceTargetDt[indexInInside] < this->accumulatedDt_ + this->dtThreshold_)
                {
                    localData.faceFluxes[indexInInside] = entries;
                }
            }
            else
            {
                // add to update vector
                updateVec[wCompIdx][eIdxGlobalI] += entries[wCompIdx];
                updateVec[nCompIdx][eIdxGlobalI] += entries[nCompIdx];
            }

            // for time step calculation
            sumfactorin += timestepFlux[0];
            sumfactorout += timestepFlux[1];

        }// end all intersections

        if (this->localTimeStepping_)
        {
            LocalTimesteppingData& localData = this->timeStepData_[eIdxGlobalI];
            for (int i=0; i < 2*dim; i++)
            {
                updateVec[wCompIdx][eIdxGlobalI] += localData.faceFluxes[i][wCompIdx];
                updateVec[nCompIdx][eIdxGlobalI] += localData.faceFluxes[i][nCompIdx];
            }
        }

        /***********     Handle source term     ***************/
        PrimaryVariables q(NAN);
        problem().source(q, element);
        updateVec[wCompIdx][eIdxGlobalI] += q[contiWEqIdx];
        updateVec[nCompIdx][eIdxGlobalI] += q[contiNEqIdx];

        // account for porosity in fluxes for time-step
        using std::max;
        sumfactorin = max(sumfactorin,sumfactorout)
        / problem().spatialParams().porosity(element);

        //calculate time step
        if (this->localTimeStepping_)
        {
            this->timeStepData_[eIdxGlobalI].dt = 1./sumfactorin;
            if ( 1./sumfactorin < dt)
            {
                dt = 1./sumfactorin;
                restrictingCell= eIdxGlobalI;
            }
        }
        else
        {
            if ( 1./sumfactorin < dt)
            {
                dt = 1./sumfactorin;
                restrictingCell= eIdxGlobalI;
            }
        }
    } // end grid traversal

    #if HAVE_MPI
    // communicate updated values
    using SolutionTypes = GetProp<TypeTag, Properties::SolutionTypes>;
    using ElementMapper = typename SolutionTypes::ElementMapper;
    using DataHandle = VectorCommDataHandleEqual<ElementMapper, Dune::BlockVector<Dune::FieldVector<Scalar, 1> >, 0/*elementCodim*/>;
    for (int i = 0; i < updateVec.size(); i++)
    {
        DataHandle dataHandle(problem_.variables().elementMapper(), updateVec[i]);
        problem_.gridView().template communicate<DataHandle>(dataHandle,
                                                             Dune::InteriorBorder_All_Interface,
                                                             Dune::ForwardCommunication);
    }

    if (this->localTimeStepping_)
    {
        using TimeDataHandle = VectorCommDataHandleEqual<ElementMapper, std::vector<LocalTimesteppingData>, 0/*elementCodim*/>;

        TimeDataHandle timeDataHandle(problem_.elementMapper(), this->timeStepData_);
        problem_.gridView().template communicate<TimeDataHandle>(timeDataHandle,
                                                                 Dune::InteriorBorder_All_Interface,
                                                                 Dune::ForwardCommunication);
    }

    dt = problem_.gridView().comm().min(dt);
    #endif

    if(impet)
    {
        Dune::dinfo << "Timestep restricted by CellIdx " << restrictingCell << " leads to dt = "
        <<dt * getParam<Scalar>("Impet.CFLFactor")<< std::endl;
        if (averagedFaces_ != 0)
            Dune::dinfo  << " Averageing done for " << averagedFaces_ << " faces. "<< std::endl;
    }
}

/*!
 * \brief  Get flux at an interface between two cells
 * The flux through \f$ \gamma \f$  is calculated according to the underlying pressure field,
 * calculated by the pressure model.
 *  \f[ - A_{\gamma} \mathbf{n}^T_{\gamma} \mathbf{K}  \sum_{\alpha} \varrho_{\alpha} \lambda_{\alpha}
     \mathbf{d}_{ij}  \left( \frac{p_{\alpha,j}^t - p^{t}_{\alpha,i}}{\Delta x} + \varrho_{\alpha} \mathbf{g}^T \mathbf{d}_{ij} \right)
                \sum_{\kappa} X^{\kappa}_{\alpha} \f]
 * Here, \f$ \mathbf{d}_{ij} \f$ is the normalized vector connecting the cell centers, and \f$ \mathbf{n}_{\gamma} \f$
 * represents the normal of the face \f$ \gamma \f$. Due to the nature of the primay Variable, the (volume-)specific
 * total mass concentration, this represents a mass flux per cell volume.
 *
 * \param fluxEntries The flux entries, mass influx from cell \f$j\f$ to \f$i\f$.
 * \param timestepFlux flow velocities for timestep estimation
 * \param intersection The intersection
 * \param cellDataI The cell data for cell \f$i\f$
 */
template<class TypeTag>
void FVTransport2P2CVEMultiDim<TypeTag>::getFlux(ComponentVector& fluxEntries,
                                        Dune::FieldVector<Scalar, 2>& timestepFlux,
                                        const Intersection& intersection,
                                        CellData& cellDataI)
{
    // access neighbor
    auto neighbor = intersection.outside();
    int eIdxGlobalJ = problem().variables().index(neighbor);
    CellData& cellDataJ = problem().variables().cellData(eIdxGlobalJ);

    const int veModelI = cellDataI.veModel();
    const int veModelJ = cellDataJ.veModel();

    if(veModelI != veModelJ)
    {
        fluxEntries = 0.;
        timestepFlux = 0.;
        // cell information
        auto elementI = intersection.inside();
        int eIdxGlobalI = problem().variables().index(elementI);

        // get position
        GlobalPosition globalPos = elementI.geometry().center();
        GlobalPosition globalPosNeighbor = neighbor.geometry().center();

        // adjust for ghost cell and domain transformation.
        Scalar localTransformVE;
        if(veModelI < 2)
        {
            auto father(neighbor);
            while(father.level() > 0)
                father = father.father();
            const Scalar localTransform = father.geometry().center()[dim-1] - problem_.aquiferHeight()/2.0;
            localTransformVE = globalPos[dim-1] - problem_.aquiferHeight()/2.0;
            globalPos[dim-1] = (globalPos[dim-1] - problem_.aquiferHeight()/2.0) + (globalPosNeighbor[dim-1] - localTransform);
        }
        else if(veModelJ < 2)
        {
            auto father(elementI);
            while(father.level() > 0)
                father = father.father();
            const Scalar localTransform = father.geometry().center()[dim-1] - problem_.aquiferHeight()/2.0;
            localTransformVE = globalPosNeighbor[dim-1] - problem_.aquiferHeight()/2.0;
            globalPosNeighbor[dim-1] = (globalPosNeighbor[dim-1] - problem_.aquiferHeight()/2.0) + (globalPos[dim-1] - localTransform);
        }

        // boundaries of ghost cell
        const CellArray numberOfCells = problem_.numberOfCells();
        const int reconstruction = getParam<int>("Grid.Reconstruction");
        const double deltaZ = problem_.aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, reconstruction));

        Scalar top;
        Scalar bottom;
        Scalar positionZ;
        if(veModelI < 2)
        {
            top = (globalPos[dim - 1] + deltaZ/2.0) - localTransformVE;
            bottom = (globalPos[dim - 1] - deltaZ/2.0) - localTransformVE;
            positionZ = globalPos[dim - 1] - localTransformVE;
        }
        else if(veModelJ < 2)
        {
            top = (globalPosNeighbor[dim - 1] + deltaZ/2.0) - localTransformVE;
            bottom = (globalPosNeighbor[dim - 1] - deltaZ/2.0) - localTransformVE;
            positionZ = globalPosNeighbor[dim - 1] - localTransformVE;
        }

        const GlobalPosition& gravity_ = problem().gravity();
        // cell volume, assume linear map here
        Scalar volume = elementI.geometry().volume();

        Scalar pcI = cellDataI.capillaryPressure();
        Scalar pcJ = cellDataJ.capillaryPressure();

        // get primary pressures
        Scalar primPressI = problem().pressureModel().pressure(eIdxGlobalI);
        Scalar primPressJ = problem().pressureModel().pressure(eIdxGlobalJ);

        // for reconstruction of pressures to work, pressures need to be set in cell data
        switch (pressureType)
        {
            case pw:
            {
                cellDataI.setPressure(wPhaseIdx, primPressI);
                cellDataI.setPressure(nPhaseIdx, primPressI + pcI);
                cellDataJ.setPressure(wPhaseIdx, primPressJ);
                cellDataJ.setPressure(nPhaseIdx, primPressJ + pcJ);
                break;
            }
            case pn:
            {
                cellDataI.setPressure(nPhaseIdx, primPressI);
                cellDataI.setPressure(wPhaseIdx, primPressI - pcI);
                cellDataJ.setPressure(nPhaseIdx, primPressJ);
                cellDataJ.setPressure(wPhaseIdx, primPressJ - pcJ);
                break;
            }
        }

        // get phase pressures
        PhaseVector pressureI;
        pressureI[wPhaseIdx] = cellDataI.pressure(wPhaseIdx);
        pressureI[nPhaseIdx] = cellDataI.pressure(nPhaseIdx);
        PhaseVector pressureJ;
        pressureJ[wPhaseIdx] = cellDataJ.pressure(wPhaseIdx);
        pressureJ[nPhaseIdx] = cellDataJ.pressure(nPhaseIdx);

        // adjust for ghost cell
        if(veModelI < 2)
        {
            pressureI[wPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, wPhaseIdx, elementI);
            pressureI[nPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, nPhaseIdx, elementI);
        }
        else if(veModelJ < 2)
        {
            pressureJ[wPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, wPhaseIdx, neighbor);
            pressureJ[nPhaseIdx] = problem_.pressureModel().reconstPressure(positionZ, nPhaseIdx, neighbor);
        }

        // adjust capillary pressure for ghost cell
        if(veModelI < 2)
            pcI = problem_.pressureModel().reconstCapillaryPressure(positionZ, elementI);
        else if(veModelJ < 2)
            pcJ = problem_.pressureModel().reconstCapillaryPressure(positionZ, neighbor);

        // fluidstate in ghost cell
        FluidState ghostFluidState;
        CompositionalFlash<Scalar, FluidSystem> flashSolver;
        ComponentVector mass(0.);
        for(int compIdx = 0; compIdx< numComponents; compIdx++)
        {
            if(veModelI < 2)
            {
                mass[compIdx] = problem_.pressureModel().concentrationIntegral(bottom, top, elementI)[compIdx]/(top-bottom);
            }
            else if(veModelJ < 2)
            {
                mass[compIdx] = problem_.pressureModel().concentrationIntegral(bottom, top, neighbor)[compIdx]/(top-bottom);
            }
        }
        Scalar Z1 = mass[0] / mass.one_norm();
        if(veModelI < 2)
        {
            flashSolver.concentrationFlash2p2c(ghostFluidState, Z1, pressureI,
                                               problem_.spatialParams().porosity(elementI), cellDataI.temperature(wPhaseIdx));
        }
        else if(veModelJ < 2)
        {
            flashSolver.concentrationFlash2p2c(ghostFluidState, Z1, pressureJ,
                                               problem_.spatialParams().porosity(neighbor), cellDataJ.temperature(wPhaseIdx));
        }

        PhaseVector saturationI;
        saturationI[wPhaseIdx] = cellDataI.saturation(wPhaseIdx);
        saturationI[nPhaseIdx] = cellDataI.saturation(nPhaseIdx);
        if(veModelI < 2)
        {
            saturationI[wPhaseIdx] = ghostFluidState.saturation(wPhaseIdx);
            saturationI[nPhaseIdx] = ghostFluidState.saturation(nPhaseIdx);
        }

        PhaseVector SmobI(0.);
        using std::max;
        SmobI[wPhaseIdx] = max((saturationI[wPhaseIdx]
        - problem().spatialParams().materialLawParams(elementI).swr())
        , 1e-2);
        SmobI[nPhaseIdx] = max((saturationI[nPhaseIdx]
        - problem().spatialParams().materialLawParams(elementI).snr())
        , 1e-2);

        PhaseVector densityI;
        densityI[wPhaseIdx] = cellDataI.density(wPhaseIdx);
        densityI[nPhaseIdx] = cellDataI.density(nPhaseIdx);
        PhaseVector densityJ;
        densityJ[wPhaseIdx] = cellDataJ.density(wPhaseIdx);
        densityJ[nPhaseIdx] = cellDataJ.density(nPhaseIdx);

        if(veModelI < 2)
        {
            densityI[wPhaseIdx] = ghostFluidState.density(wPhaseIdx);
            densityI[nPhaseIdx] = ghostFluidState.density(nPhaseIdx);
        }
        else if(veModelJ < 2)
        {
            densityJ[wPhaseIdx] = ghostFluidState.density(wPhaseIdx);
            densityJ[nPhaseIdx] = ghostFluidState.density(nPhaseIdx);
        }

        // face properties
        GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
        if (switchNormals)
            unitOuterNormal *= -1.0;
        Scalar faceArea = intersection.geometry().volume();

        // create vector for timestep and for update
        Dune::FieldVector<Scalar, 2> factor (0.);
        Dune::FieldVector<Scalar, 2> updFactor (0.);

        PhaseVector potential(0.);

        // distance vector between barycenters
        GlobalPosition distVec = globalPosNeighbor - globalPos;
        // compute distance between cell centers
        Scalar dist = distVec.two_norm();

        GlobalPosition unitDistVec(distVec);
        unitDistVec /= dist;

        // average phase densities with central weighting
        double densityW_mean = (densityI[wPhaseIdx] + densityJ[wPhaseIdx]) * 0.5;
        double densityNW_mean = (densityI[nPhaseIdx] + densityJ[nPhaseIdx]) * 0.5;

        DimMatrix K_I(problem().spatialParams().intrinsicPermeability(elementI));

        // compute mean permeability
        DimMatrix meanK_(0.);
        harmonicMeanMatrix(meanK_,
                           K_I,
                           problem().spatialParams().intrinsicPermeability(neighbor));
        Dune::FieldVector<Scalar,dim> K(0);
        meanK_.umv(unitDistVec,K);

        // determine potentials for upwind
        potential[wPhaseIdx] = (pressureI[wPhaseIdx] - pressureJ[wPhaseIdx]) / (dist);
        potential[nPhaseIdx] = (pressureI[nPhaseIdx] - pressureJ[nPhaseIdx]) / (dist);

        // add gravity term
        potential[nPhaseIdx] +=  (gravity_ * unitDistVec) * densityNW_mean;
        potential[wPhaseIdx] +=  (gravity_ * unitDistVec) * densityW_mean;

        potential[wPhaseIdx] *= fabs(K * unitOuterNormal);
        potential[nPhaseIdx] *= fabs(K * unitOuterNormal);

        // get mobilities
        PhaseVector mobilityI;
        mobilityI[wPhaseIdx] = cellDataI.mobility(wPhaseIdx);
        mobilityI[nPhaseIdx] = cellDataI.mobility(nPhaseIdx);
        PhaseVector mobilityJ;
        mobilityJ[wPhaseIdx] = cellDataJ.mobility(wPhaseIdx);
        mobilityJ[nPhaseIdx] = cellDataJ.mobility(nPhaseIdx);

        // adjust for ghost cell
        if(veModelI < 2)
        {
            mobilityI[wPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, elementI, wPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(wPhaseIdx);
            mobilityI[nPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, elementI, nPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(nPhaseIdx);
        }
        else if(veModelJ < 2)
        {
            mobilityJ[wPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, neighbor, wPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(wPhaseIdx);
            mobilityJ[nPhaseIdx] = problem_.pressureModel().relPermeabilityIntegral(bottom, top, neighbor, nPhaseIdx)
            / (top-bottom) / ghostFluidState.viscosity(nPhaseIdx);
        }

        // get mass fractions
        PhaseVector massFractionIW, massFractionIN;
        massFractionIW[wPhaseIdx] = cellDataI.massFraction(wPhaseIdx, wCompIdx);
        massFractionIW[nPhaseIdx] = cellDataI.massFraction(nPhaseIdx, wCompIdx);
        massFractionIN[wPhaseIdx] = cellDataI.massFraction(wPhaseIdx, nCompIdx);
        massFractionIN[nPhaseIdx] = cellDataI.massFraction(nPhaseIdx, nCompIdx);
        ComponentVector massFractionJW, massFractionJN;
        massFractionJW[wPhaseIdx] = cellDataJ.massFraction(wPhaseIdx, wCompIdx);
        massFractionJW[nPhaseIdx] = cellDataJ.massFraction(nPhaseIdx, wCompIdx);
        massFractionJN[wPhaseIdx] = cellDataJ.massFraction(wPhaseIdx, nCompIdx);
        massFractionJN[nPhaseIdx] = cellDataJ.massFraction(nPhaseIdx, nCompIdx);

        if(veModelI < 2)
        {
            massFractionIW[wPhaseIdx] = ghostFluidState.massFraction(wPhaseIdx, wCompIdx);
            massFractionIW[nPhaseIdx] = ghostFluidState.massFraction(nPhaseIdx, wCompIdx);
            massFractionIN[wPhaseIdx] = ghostFluidState.massFraction(wPhaseIdx, nCompIdx);
            massFractionIN[nPhaseIdx] = ghostFluidState.massFraction(nPhaseIdx, nCompIdx);
        }
        else if(veModelJ < 2)
        {
            massFractionJW[wPhaseIdx] = ghostFluidState.massFraction(wPhaseIdx, wCompIdx);
            massFractionJW[nPhaseIdx] = ghostFluidState.massFraction(nPhaseIdx, wCompIdx);
            massFractionJN[wPhaseIdx] = ghostFluidState.massFraction(wPhaseIdx, nCompIdx);
            massFractionJN[nPhaseIdx] = ghostFluidState.massFraction(nPhaseIdx, nCompIdx);
        }

        // determine upwinding direction, perform upwinding if possible
        Dune::FieldVector<bool, NumPhases> doUpwinding(true);
        PhaseVector lambda(0.);
        for(int phaseIdx = 0; phaseIdx < NumPhases; phaseIdx++)
        {
            int contiEqIdx = 0;
            if(phaseIdx == wPhaseIdx)
                contiEqIdx = contiWEqIdx;
            else
                contiEqIdx = contiNEqIdx;

            if(!this->impet_ || restrictFluxInTransport_==0) // perform a strict uwpind scheme
            {
                if (potential[phaseIdx] > 0.)
                {
                    lambda[phaseIdx] = mobilityI[phaseIdx];
                    cellDataI.setUpwindCell(intersection.indexInInside(), contiEqIdx, true);

                }
                else if(potential[phaseIdx] < 0.)
                {
                    lambda[phaseIdx] = mobilityJ[phaseIdx];
                    cellDataI.setUpwindCell(intersection.indexInInside(), contiEqIdx, false);
                }
                else
                {
                    doUpwinding[phaseIdx] = false;
                    cellDataI.setUpwindCell(intersection.indexInInside(), contiEqIdx, false);
                    cellDataJ.setUpwindCell(intersection.indexInOutside(), contiEqIdx, false);
                }
            }
            else // Transport after PE with check on flow direction
            {
                bool wasUpwindCell = cellDataI.isUpwindCell(intersection.indexInInside(), contiEqIdx);

                if (potential[phaseIdx] > 0. && wasUpwindCell)
                    lambda[phaseIdx] = mobilityI[phaseIdx];
                else if (potential[phaseIdx] < 0. && !wasUpwindCell)
                    lambda[phaseIdx] = mobilityJ[phaseIdx];
                // potential direction does not coincide with that of P.E.
                else if(restrictFluxInTransport_ == 2)   // perform central averaging for all direction changes
                    doUpwinding[phaseIdx] = false;
                else    // i.e. restrictFluxInTransport == 1
                {
                    //check if harmonic weighting is necessary
                    if (potential[phaseIdx] > 0. && (Dune::FloatCmp::ne<Scalar,Dune::FloatCmp::absolute>
                        (mobilityJ[phaseIdx], 0.0, 1.0e-30)   // check if outflow induce neglected (i.e. mob=0) phase flux
                        || (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())))
                        lambda[phaseIdx] = mobilityI[phaseIdx];
                    else if (potential[phaseIdx] < 0. && (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>
                        (mobilityI[phaseIdx], 0.0, 1.0e-30) // check if inflow induce neglected phase flux
                        || (cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())))
                        lambda[phaseIdx] = mobilityJ[phaseIdx];
                    else
                        doUpwinding[phaseIdx] = false;
                }

            }

            // do not perform upwinding if so desired
            if(!doUpwinding[phaseIdx])
            {
                //a) no flux if there wont be any flux regardless how to average/upwind
                if(mobilityI[phaseIdx]+mobilityJ[phaseIdx]==0.)
                {
                    potential[phaseIdx] = 0;
                    continue;
                }

                //b) perform harmonic averaging
                fluxEntries[wCompIdx] -= potential[phaseIdx] * faceArea / volume
                * harmonicMean(massFractionIW[phaseIdx] * mobilityI[phaseIdx] * densityI[phaseIdx],
                               massFractionJW[phaseIdx] * mobilityJ[phaseIdx] * densityJ[phaseIdx]);
                fluxEntries[nCompIdx] -= potential[phaseIdx] * faceArea / volume
                * harmonicMean(massFractionIN[phaseIdx] * mobilityI[phaseIdx] * densityI[phaseIdx],
                               massFractionJN[phaseIdx] * mobilityJ[phaseIdx] * densityJ[phaseIdx]);
                // c) timestep control
                // for timestep control : influx
                using std::max;
                timestepFlux[0] += max(0.,
                                       - potential[phaseIdx] * faceArea / volume
                                       * harmonicMean(mobilityI[phaseIdx],mobilityJ[phaseIdx]));
                // outflux
                timestepFlux[1] += max(0.,
                                       potential[phaseIdx] * faceArea / volume
                                       * harmonicMean(mobilityI[phaseIdx],mobilityJ[phaseIdx])/SmobI[phaseIdx]);

                //d) output
                if(!(cellDataI.wasRefined() && cellDataJ.wasRefined() && elementI.father() == neighbor.father())
                    && eIdxGlobalI > eIdxGlobalJ) //(only for one side)
                    {
                        averagedFaces_++;
                        #if DUNE_MINIMAL_DEBUG_LEVEL < 3
                        // verbose (only for one side)
                        if(eIdxGlobalI > eIdxGlobalJ)
                            Dune::dinfo << "harmonicMean flux of phase" << phaseIdx <<" used from cell" << eIdxGlobalI<< " into " << eIdxGlobalJ
                            << " ; TE upwind I = "<< cellDataI.isUpwindCell(intersection.indexInInside(), contiEqIdx)
                            << " but pot = "<< potential[phaseIdx] <<  std::endl;
                        #endif
                    }

                    //e) stop further standard calculations
                    potential[phaseIdx] = 0;
            }
        }

        // calculate and standardized velocity
        using std::max;
        double velocityJIw = max((-lambda[wPhaseIdx] * potential[wPhaseIdx]) * faceArea / volume, 0.0);
        double velocityIJw = max(( lambda[wPhaseIdx] * potential[wPhaseIdx]) * faceArea / volume, 0.0);
        double velocityJIn = max((-lambda[nPhaseIdx] * potential[nPhaseIdx]) * faceArea / volume, 0.0);
        double velocityIJn = max(( lambda[nPhaseIdx] * potential[nPhaseIdx]) * faceArea / volume, 0.0);

        // for timestep control : influx
        timestepFlux[0] += velocityJIw + velocityJIn;

        double foutw = velocityIJw/SmobI[wPhaseIdx];
        double foutn = velocityIJn/SmobI[nPhaseIdx];
        using std::isnan;
        using std::isinf;
        if (isnan(foutw) || isinf(foutw) || foutw < 0) foutw = 0;
        if (isnan(foutn) || isinf(foutn) || foutn < 0) foutn = 0;
        timestepFlux[1] += foutw + foutn;

        fluxEntries[wCompIdx] +=
        velocityJIw * massFractionJW[wPhaseIdx] * densityJ[wPhaseIdx]
        - velocityIJw * massFractionIW[wPhaseIdx] * densityI[wPhaseIdx]
        + velocityJIn * massFractionJW[nPhaseIdx] * densityJ[nPhaseIdx]
        - velocityIJn * massFractionIW[nPhaseIdx] * densityI[nPhaseIdx];
        fluxEntries[nCompIdx] +=
        velocityJIw * massFractionJN[wPhaseIdx] * densityJ[wPhaseIdx]
        - velocityIJw * massFractionIN[wPhaseIdx] * densityI[wPhaseIdx]
        + velocityJIn * massFractionJN[nPhaseIdx] * densityJ[nPhaseIdx]
        - velocityIJn * massFractionIN[nPhaseIdx] * densityI[nPhaseIdx];
    }
    else
    {
        ParentType::getFlux(fluxEntries, timestepFlux, intersection, cellDataI);
    }
    return;
}

} // end namespace Dumux
#endif
