add_input_file_links()

dune_add_test(NAME test_storage2p2cve
              SOURCES test_storage2p2cve.cc
                            TIMEOUT 500
              COMMAND ${CMAKE_SOURCE_DIR}/../dumux/bin/testing/runtest.py
              CMD_ARGS --script fuzzy
                       --files ${CMAKE_SOURCE_DIR}/test/references/testStorage2p2cve-gridAfterReconstruction-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/gridAfterReconstruction-00001.vtu
                               ${CMAKE_SOURCE_DIR}/test/references/testStorage2p2cve-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/test_storage2p2cve-00002.vtu
                       --command "${CMAKE_CURRENT_BINARY_DIR}/test_storage2p2cve")
