// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test problem for the sequential 2p model
 */
#ifndef DUMUX_TEST_STORAGE2P2CVE_PROBLEM_HH
#define DUMUX_TEST_STORAGE2P2CVE_PROBLEM_HH

#include <dune/grid/uggrid.hh>

#include <dumux/io/gnuplotinterface.hh>

#include <dumux/material/fluidsystems/h2och4.hh>
#include <dumux/material/fluidsystems/h2oh2.hh>
#include <dumux/material/fluidsystems/h2oh2tabulated.hh>
#include <dumux/material/components/h2tables.hh>

#include <dumux/porousmediumflow/2p2c/sequential/problem.hh>
#include <dumux/decoupled/2p2cve/fvpressure2p2cve.hh>
#include <dumux/decoupled/2p2cve/celldata2p2cve.hh>
#include <dumux/decoupled/2p2cve/fvtransport2p2cve.hh>

#include "test_storage2p2cvespatialparams.hh"

namespace Dumux
{

template<class TypeTag>
class Storage2P2CTestProblem;

//////////
// Specify the properties
//////////
namespace Properties
{
// Create new type tags
namespace TTag {
    struct Storage2P2CVETest { using InheritsFrom = std::tuple<Storage2P2CVESpatialParams, SequentialTwoPTwoC>; };
} // end namespace TTag


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Storage2P2CVETest> { using type = Dune::UGGrid<3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Storage2P2CVETest> { using type = Storage2P2CTestProblem<TypeTag>; };

// Set the cell data
template<class TypeTag>
struct CellData<TypeTag, TTag::Storage2P2CVETest> { using type = CellData2P2CVE<TypeTag>; };

//Set the transport model
template<class TypeTag>
struct TransportModel<TypeTag, TTag::Storage2P2CVETest> { using type = FVTransport2P2CVE<TypeTag>; };

//Set the pressure model
template<class TypeTag>
struct PressureModel<TypeTag, TTag::Storage2P2CVETest> { using type = FVPressure2P2CVE<TypeTag>; };

////////////////////////////////////////////////////////////////////////
//Switch to a p_n-S_w formulation
//
//SET_INT_PROP(IMPESTestProblem, Formulation,
//        DecoupledTwoPCommonIndices::pnsn);
//
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//Switch to a p_global-S_w formulation
//
//SET_INT_PROP(IMPESTestProblem, Formulation,
//        DecoupledTwoPCommonIndices::pGlobalSw);
//
//Define the capillary pressure term in the transport equation -> only needed in case of a p_global-S_w formulation!
//SET_TYPE_PROP(IMPESTestProblem, CapillaryFlux, CapillaryDiffusion<TypeTag>);
//
//Define the gravity term in the transport equation -> only needed in case of a p_global-S_w formulation!
//SET_TYPE_PROP(IMPESTestProblem, GravityFlux, GravityPart<TypeTag>);
//
////////////////////////////////////////////////////////////////////////

// Select fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Storage2P2CVETest>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::H2OH2Tabulated<Scalar, H2Tables::H2Tables>;
};

//Set the grid view to level grid view by manipulating the grid geometry
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::Storage2P2CVETest>
{
    using GV = typename GetPropType<TypeTag, Properties::Grid>::LevelGridView;
    struct MockFVGridGeometry
    : public DefaultMapperTraits<GV>
    {
        static constexpr Dumux::DiscretizationMethod discMethod = Dumux::DiscretizationMethod::cctpfa;
        using GridView = GV;
    };
public:
    using type = MockFVGridGeometry;
};

//Set the CFL flux function
// template<class TypeTag>
// struct EvalCflFluxFunction<TypeTag, TTag::Storage2P2CVETest> { using type = EvalCflFluxCoats<TypeTag>; };

template<class TypeTag>
struct EnableCapillarity<TypeTag, TTag::Storage2P2CVETest> { static constexpr bool value = true; };
template<class TypeTag>
struct BoundaryMobility<TypeTag, TTag::Storage2P2CVETest> {
    static constexpr int value = GetPropType<TypeTag, Properties::Indices>::satDependent; };
template<class TypeTag>
struct PressureFormulation<TypeTag, TTag::Storage2P2CVETest> {
    static constexpr int value = GetPropType<TypeTag, Properties::Indices>::pressureW; };
}
/*!
 * \ingroup 2PVETest
 *
 * \brief test problem for the sequential 2p model
 *
 * Water is injected from the left side into a rectangular 2D domain also
 * filled with water. Upper and lower boundary is closed (Neumann = 0),
 * and there is free outflow on the right side.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_impes -parameterFile ./test_impes.input</tt>,
 * where the arguments define the parameter file..
 */
template<class TypeTag>
class Storage2P2CTestProblem: public IMPETProblem2P2C<TypeTag>
{
    using ParentType = IMPETProblem2P2C<TypeTag>;
    friend class IMPETProblem<TypeTag>;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using LeafGridView = typename Grid::LeafGridView;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using WettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::H2O;
    using NonWettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::H2;

    using TimeManager = GetPropType<TypeTag, Properties::TimeManager>;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wPhaseIdx,
        nCompIdx = Indices::nPhaseIdx,
        numComponents = getPropValue<TypeTag, Properties::NumComponents>()
    };

    enum VEModel
    {
        sharpInterface,
        capillaryFringe
    };

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;
    using GlobalPosition = typename Dune::FieldVector<Scalar, dimWorld>;

    using CellData = GetPropType<TypeTag, Properties::CellData>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;
    using SolutionTypes = GetProp<TypeTag, Properties::SolutionTypes>;
    using PrimaryVariables = typename SolutionTypes::PrimaryVariables;

    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

    using CellArray = std::array<unsigned int, dimWorld>;

public:
    Storage2P2CTestProblem(TimeManager &timeManager, Grid &grid) :
    ParentType(timeManager, grid, grid.levelGridView(0)), eps_(1e-6), visualizationWriter_(grid.leafGridView(), "gridAfterReconstruction"),
    normWriter_(grid.leafGridView(), "gridAfterRefinement")
    {
        // initialize the tables of the fluid system
        FluidSystem::init(280.0, 400.0, 121, 1e5, 1000e5, 1000);

        int outputInterval = 1e9;
        if (hasParam("Problem.OutputInterval"))
        {
            outputInterval = getParam<Scalar>("Problem.OutputInterval");
        }
        this->setOutputInterval(outputInterval);

        if (hasParam("Problem.OutputTimeInterval"))
        {
            Scalar outputTimeInterval = getParam<Scalar>("Problem.OutputTimeInterval");
            this->setOutputTimeInterval(outputTimeInterval);
        }

        this->spatialParams().plotMaterialLaw();

        name_ = getParam<std::string>("Problem.Name");

        if (hasParam("Grid.UpperRight"))
            aquiferHeight_ = getParam<std::vector<Scalar>>("Grid.UpperRight")[dim-1];
        else if (hasParam("Grid.Axial" +  std::to_string(dim-1)))
            aquiferHeight_ = getParam<std::vector<Scalar>>("Grid.Axial" +  std::to_string(dim-1)).back();
        else
            DUNE_THROW(Dune::GridError, "Unknown grid specification. Domain height could not be identified.");

        if (hasParam("Grid.UpperRight"))
            aquiferLength_ = getParam<std::vector<Scalar>>("Grid.UpperRight")[0];
        else if (hasParam("Grid.Radial" +  std::to_string(0)))
            aquiferLength_ = getParam<std::vector<Scalar>>("Grid.Radial" +  std::to_string(0)).back();
        else
            DUNE_THROW(Dune::GridError, "Unknown grid specification. Domain length could not be identified.");

        const int level = getParam<Scalar>("Grid.Reconstruction");

        // store pointer to all ve-columns in a map, indicated by their global index
        // iterate over all elements
        for (const auto& element : Dune::elements(this->gridView()))
        {
            // column number equals global Index
            int eIdxGlobal = this->variables().index(element);

            mapColumns_.insert(std::make_pair(eIdxGlobal, element));
            dummy_ = element;
        }

        //calculate length of capillary transition zone (CTZ)
        Scalar swr = this->spatialParams().materialLawParams(dummy_).swr();
        Scalar snr = this->spatialParams().materialLawParams(dummy_).snr();
        Scalar satW1 = 1.0 - snr;
        Scalar satW2 = swr + 0.1*(1.0-swr-snr);
        Scalar pc1 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW1);
        Scalar pc2 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW2);
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar tempRef = temperatureAtPos(globalPos);
        Scalar densityW = WettingPhase::liquidDensity(tempRef, pRef);
        Scalar densityN = NonWettingPhase::gasDensity(tempRef, pRef);
        Scalar gravity = this->gravity().two_norm();
        CTZ_ = (pc2-pc1)/((densityW-densityN)*gravity);
        std::cout << "CTZ " << CTZ_ << std::endl;

        LeafGridView visualizationGridView = this->grid().leafGridView();
        for(int i=0;i<level;i++)
        {
            for (const auto& element : Dune::elements(visualizationGridView))
            {
                this->grid().mark(element, UG::D3::HEX_BISECT_0_3, 0);
            }
            // adapt the grid
            this->grid().preAdapt();
            this->grid().adapt();
            this->grid().postAdapt();
        }

        residualSegSaturation_ = 1.0;
        residualSegSatAverage_ = 1.0;
        corrGasPlumeHeight_ = 0.0;
        corrGasPlumeHeightOld_ = 0.0;
        gasPlumeTipOld_ = 0.0;
        averageSat_ = 0.0;
        averageSatOld_ = 0.0;
        isSegregated_ = false;

        //reset all output
        system("rm *.out");
    }

    void postTimeStep()
    {
        ParentType::postTimeStep();

        int outputInterval = 1e9;
        if (hasParam("Problem.OutputInterval"))
        {
            outputInterval = getParam<Scalar>("Problem.OutputInterval");
        }

        const CellArray numberOfCells = this->numberOfCells();
        const int reconstruction = getParam<int>("Grid.Reconstruction");
        const double deltaX = this->aquiferLength()/numberOfCells[0];
        const double deltaZ = this->aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, reconstruction));

        //write output for reconstructed solution
        if(this->timeManager().timeStepIndex() % outputInterval == 0 || this->timeManager().willBeFinished()
            || this->timeManager().episodeWillBeFinished() || this->timeManager().timeStepIndex() == 0)
        {
            LeafGridView visualizationGridView = this->grid().leafGridView();
            // use visualization writer
            visualizationWriter_.gridChanged();
            visualizationWriter_.beginWrite(this->timeManager().time() + this->timeManager().timeStepSize());
            //write stuff out
            using ScalarSolution = typename SolutionTypes::ScalarSolution;

            int size = visualizationGridView.size(0);
            ScalarSolution *totalConcentrationW = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *totalConcentrationN = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *pressureW = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *pressureNw = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *capillaryPressure = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *saturationW = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *saturationNw = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *mobilityW = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *mobilityNw = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *densityW = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *densityNw = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *massFractionWW = visualizationWriter_.allocateManagedBuffer (size);
            ScalarSolution *massFractionNW = visualizationWriter_.allocateManagedBuffer (size);

            for (const auto& element : Dune::elements(visualizationGridView))
            {
                const GlobalPosition& globalPos = element.geometry().center();
                const int eIdxGlobal = this->variables().index(element);
                const int j = round((globalPos[0] - (deltaX/2.0))/deltaX);

                const Element& veElement = mapColumns_.find(j)->second;
                const GlobalPosition& globalPosVE = veElement.geometry().center();
                const int eIdxGlobalVE = this->variables().index(veElement);
                const CellData& cellData = this->variables().cellData(eIdxGlobalVE);

                const Scalar localTransform = globalPosVE[dim-1] - this->aquiferHeight()/2.0;
                const Scalar top = (globalPos[dim - 1] + deltaZ/2.0) - localTransform;
                const Scalar bottom = (globalPos[dim - 1] - deltaZ/2.0) - localTransform;
                const Scalar positionZ = globalPos[dim - 1] - localTransform;

                const Dune::FieldVector<Scalar,numComponents> totalConcentration =
                this->pressureModel().concentrationIntegral(bottom, top, veElement)/(top-bottom);

                const Scalar satW = this->pressureModel().saturationIntegral(bottom, top, veElement)/(top-bottom);
                const Scalar satNw = 1.0-satW;

                (*totalConcentrationW)[eIdxGlobal] = totalConcentration[wCompIdx];
                (*totalConcentrationN)[eIdxGlobal] = totalConcentration[nCompIdx];
                (*pressureW)[eIdxGlobal] = this->pressureModel().reconstPressure(positionZ, wPhaseIdx, veElement);
                (*pressureNw)[eIdxGlobal] = this->pressureModel().reconstPressure(positionZ, nPhaseIdx, veElement);
                (*capillaryPressure)[eIdxGlobal] = this->pressureModel().reconstCapillaryPressure(positionZ, veElement);
                (*saturationW)[eIdxGlobal] = satW;
                (*saturationNw)[eIdxGlobal] = satNw;
                (*mobilityW)[eIdxGlobal] = this->pressureModel().relPermeabilityIntegral(bottom, top, veElement, wPhaseIdx)
                / (top-bottom) / cellData.viscosity(wPhaseIdx);
                (*mobilityNw)[eIdxGlobal] = this->pressureModel().relPermeabilityIntegral(bottom, top, veElement, nPhaseIdx)
                / (top-bottom) / cellData.viscosity(nPhaseIdx);
                (*densityW)[eIdxGlobal] = this->pressureModel().densityLiquidAverage(bottom, top, veElement);
                (*densityNw)[eIdxGlobal] = cellData.density(nPhaseIdx);
                (*massFractionWW)[eIdxGlobal] = this->pressureModel().massFractionLiquidAverage(bottom, top, veElement)[wCompIdx];
                (*massFractionNW)[eIdxGlobal] = cellData.massFraction(nPhaseIdx, wCompIdx);
            }

            visualizationWriter_.attachCellData(*totalConcentrationW, "total concentration W");
            visualizationWriter_.attachCellData(*totalConcentrationN, "total concentration N");
            visualizationWriter_.attachCellData(*pressureW, "wetting pressure");
            visualizationWriter_.attachCellData(*pressureNw, "non-wetting pressure");
            visualizationWriter_.attachCellData(*capillaryPressure, "capillary pressure");
            visualizationWriter_.attachCellData(*saturationW, "wetting saturation");
            visualizationWriter_.attachCellData(*saturationNw, "non-wetting saturation");
            visualizationWriter_.attachCellData(*mobilityW, "wetting mobility");
            visualizationWriter_.attachCellData(*mobilityNw, "non-wetting mobility");
            visualizationWriter_.attachCellData(*densityW, "wetting density");
            visualizationWriter_.attachCellData(*densityNw, "non-wetting density");
            visualizationWriter_.attachCellData(*massFractionWW, "mass fraction W in wetting phase");
            visualizationWriter_.attachCellData(*massFractionNW, "mass fraction W in non-wetting phase");
            visualizationWriter_.endWrite();
        }
//
// //             //refine grid for error norm calculation
// //             this->grid().preAdapt();
// //             const int additionalRefinementSteps = 2;
// //             for(int i=0;i<additionalRefinementSteps;i++)
// //             {
// //                 for (const auto& element : Dune::elements(visualizationGridView))
// //                 {
// //                     GlobalPosition globalPos = element.geometry().center();
// //                     this->grid().mark(1, element);
// //                 }
// //                 // adapt the grid
// //                 this->grid().adapt();
// //                 this->grid().postAdapt();
// //             }
// //             int reconstruction = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, Reconstruction);
// //             const double deltaZRefined = this->aquiferHeight()/(numberOfCells[dim - 1]
// //             *std::pow(2, (reconstruction+additionalRefinementSteps)));
// //
// //             // use error norm writer
// //             normWriter_.gridChanged();
// //             normWriter_.beginWrite(this->timeManager().time() + this->timeManager().timeStepSize());
// //
// //             size = visualizationGridView.size(0);
// //             ScalarSolution *pressureW1 = normWriter_.allocateManagedBuffer (size);
// //             ScalarSolution *pressureNw1 = normWriter_.allocateManagedBuffer (size);
// //             ScalarSolution *capillaryPressure1 = normWriter_.allocateManagedBuffer (size);
// //             ScalarSolution *saturationW1 = normWriter_.allocateManagedBuffer (size);
// //             ScalarSolution *saturationNw1 = normWriter_.allocateManagedBuffer (size);
// //             ScalarSolution *mobilityW1 = normWriter_.allocateManagedBuffer (size);
// //             ScalarSolution *mobilityNw1 = normWriter_.allocateManagedBuffer (size);
// //
// //             //write output on refined grid
// //             for (const auto& element : Dune::elements(visualizationGridView))
// //             {
// //                 //identify column number of element and check if it is a VE column or a full-d column
// //                 GlobalPosition globalPos = element.geometry().center();
// //                 int columnNumber = floor(globalPos[0]/deltaX);//starting with 0
// //
// //                 //identify ID of cell, same as ID of parallel run with 20 blocks
// //                 //determine block number starting from 0
// //                 int blockNumberX = floor(globalPos[0]/35);
// //                 int blockNumberY = floor(globalPos[dim-1]/15);
// //                 int blockNumber = blockNumberY*10 + blockNumberX;
// //                 //determine local cell ID in block, starting from 0
// //                 Scalar localX = globalPos[0] - (blockNumberX*35);
// //                 Scalar localY = globalPos[dim-1] - (blockNumberY*15);
// //                 int localNumberX = floor(localX/0.25);
// //                 int localNumberY = floor(localY/(30.0/512.0));
// //                 int localID = localNumberY*140 + localNumberX;
// //                 //determine global ID, starting from 0
// //                 int globalID = blockNumber*35840 + localID;
// //
// //                 int j = round((globalPos[0] - (deltaX/2.0))/deltaX);
// //                 Element veElement = mapColumns_.find(j)->second;
// //
// //                 const double deltaZRefined = this->aquiferHeight()/(numberOfCells[dim - 1]
// //                 *std::pow(2, (reconstruction+additionalRefinementSteps)));
// //                 Scalar top = globalPos[dim - 1] + deltaZRefined/2.0;
// //                 Scalar bottom = globalPos[dim - 1] - deltaZRefined/2.0;
// //
// //                 Scalar satW = this->pressureModel().saturationIntegral(bottom, top, veElement)/(top-bottom);
// //                 Scalar satNw = 1.0-satW;
// //                 Scalar pRef = referencePressureAtPos(globalPos);
// //                 Scalar temp = temperatureAtPos(globalPos);
// //
// //                 (*pressureW1)[globalID] = this->pressureModel().reconstPressure(globalPos[dim-1], wPhaseIdx, veElement);
// //                 (*pressureNw1)[globalID] = this->pressureModel().reconstPressure(globalPos[dim-1], nPhaseIdx, veElement);
// //                 (*capillaryPressure1)[globalID] = this->pressureModel().reconstCapillaryPressure(globalPos[dim-1], veElement);
// //                 (*saturationW1)[globalID] = satW;
// //                 (*saturationNw1)[globalID] = satNw;
// //                 (*mobilityW1)[globalID] = this->pressureModel().calculateRelPermeabilityCoarse(bottom, top, veElement, wPhaseIdx)
// //                                           /WettingPhase::viscosity(temp, pRef);
// //                 (*mobilityNw1)[globalID] = this->pressureModel().calculateRelPermeabilityCoarse(bottom, top, veElement, nPhaseIdx)
// //                                            /NonWettingPhase::viscosity(temp, pRef);
// //             }
// //             normWriter_.attachCellData(*pressureW1, "wetting pressure");
// //             normWriter_.attachCellData(*pressureNw1, "non-wetting pressure");
// //             normWriter_.attachCellData(*capillaryPressure1, "capillary pressure");
// //             normWriter_.attachCellData(*saturationW1, "wetting saturation");
// //             normWriter_.attachCellData(*saturationNw1, "non-wetting saturation");
// //             normWriter_.attachCellData(*mobilityW1, "wetting mobility");
// //             normWriter_.attachCellData(*mobilityNw1, "non-wetting mobility");
// //             normWriter_.endWrite();
// //
// //             //coarsen grid after error norm calculation
// //             for(int i=0;i<additionalRefinementSteps;i++)
// //             {
// //                 for (const auto& element : Dune::elements(visualizationGridView))
// //                 {
// //                     GlobalPosition globalPos = element.geometry().center();
// //                     this->grid().mark(-1, element);
// //                 }
// //                 // adapt the grid
// //                 this->grid().adapt();
// //                 this->grid().postAdapt();
// //                 this->grid().preAdapt();
// //             }
// //         }

        if (getParam<bool>("Output.PlotDissolvedMass"))
        {
            bool plotCriteria = false;
            const int timeStepIdx = this->timeManager().timeStepIndex();
            //only plot VE criteria over domain for specific timeStepIdx
            if (timeStepIdx % 10 == 0)
            {
                plotCriteria = true;
            }

            if(plotCriteria)
            {
                Scalar totalMassGas = 0.0;
                Scalar totalMassDissolvedGas = 0.0;
                //iterate over all cells
                for (const auto& element : elements(this->gridView()))
                {
                    const Scalar volume = element.geometry().volume();
                    const GlobalPosition globalPos = element.geometry().center();
                    const int eIdxGlobal = this->variables().index(element);
                    CellData& cellData = this->variables().cellData(eIdxGlobal);

                    totalMassGas += volume * cellData.totalConcentration(nCompIdx);
                    totalMassDissolvedGas += cellData.saturation(wPhaseIdx) * volume
                    * this->spatialParams().porosity(element) * cellData.density(wPhaseIdx)
                    * cellData.massFraction(wPhaseIdx, nCompIdx);
                }

                //write data to mass-plots
                std::ostringstream oss;
                oss << "massDissolvedGasVE" << ".out";
                std::string fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << this->timeManager().time() << " " << totalMassDissolvedGas <<  " " <<  totalMassDissolvedGas/totalMassGas*100 << std::endl;
                outputFile_.close();
            }
        }

        if (getParam<bool>("Output.PlotOperationOverTime"))
        {
            Scalar gasPressureAtWell = 0.0;
            Scalar waterInGasAtWell = 0.0;
            Scalar saturationAtWell = 0.0;
            for (const auto& element : elements(this->gridView()))
            {
                for (const auto& intersection : intersections(this->gridView(), element))
                {
                    BoundaryTypes bcType;
                    this->boundaryTypes(bcType, intersection);
                    if (bcType.isNeumann(Indices::contiNEqIdx))
                    {
                        const GlobalPosition &globalPos = intersection.geometry().center();
                        // plot pressure next to well
                        if(isWell(globalPos))
                        {
                            const auto element = intersection.inside();
                            const int eIdxGlobal = this->variables().index(element);
                            const CellData& cellData = this->variables().cellData(eIdxGlobal);
                            const CellArray numberOfCells = this->numberOfCells();
                            const int reconstruction = getParam<int>("Grid.Reconstruction");
                            const double deltaZ = this->aquiferHeight()/(numberOfCells[dim - 1]*std::pow(2, reconstruction));
                            const Scalar localTransform = globalPos[dim-1] - this->aquiferHeight()/2.0;
                            static const Scalar wellHeight = getParam<double>("BoundaryConditions.WellHeight");
                            const Scalar positionZ = wellHeight - localTransform + deltaZ/2.0;
                            gasPressureAtWell = this->pressureModel().reconstPressure(positionZ, nPhaseIdx, element);
                            waterInGasAtWell = cellData.massFraction(nPhaseIdx, wCompIdx);
                            saturationAtWell = this->pressureModel().reconstSaturation(positionZ, nPhaseIdx, element);
                        }
                    }
                }
            }

            //write data to plot
            std::ostringstream oss;
            oss << "operationOverTimeVE" << ".out";
            std::string fileName = oss.str();
            outputFile_.open(fileName, std::ios::app);
            outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()
            << " " << gasPressureAtWell
            << " " << waterInGasAtWell
            << " " << saturationAtWell << std::endl;
            outputFile_.close();
        }
    }

/*! \brief Calculates new residual saturation for VE-model depending on segregation time
 */
void calculateResidualSegSaturation(Scalar gasPlumeTip, Scalar corrGasPlumeHeight, Scalar averageSatOldLocation, Scalar averageSat)
{
    int correction = 0;
    if (hasParam("VE.correction"))
    {
        correction = getParam<int>("VE.correction");
    }

    Scalar segSatW;
    if(correction == 1 || correction == 2 || correction == 3 || correction == 4)
    {
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar tempRef = temperatureAtPos(globalPos);
        Scalar aquiferHeight = this->aquiferHeight();
        Scalar charHeight = aquiferHeight;
        Scalar porosity = this->spatialParams().porosity(dummy_);
        Scalar viscosityW = WettingPhase::viscosity(tempRef, pRef);
        Scalar viscosityNw = NonWettingPhase::viscosity(tempRef, pRef);
        Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        Scalar permeability = getParam<Scalar>("SpatialParams.PermeabilityVertical");
        Scalar densityW = WettingPhase::density(tempRef, pRef);
        Scalar densityN = NonWettingPhase::density(tempRef, pRef);
        Scalar gravity = this->gravity().two_norm();
        Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
        Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
        Scalar exponent = getParam<Scalar>("SpatialParams.Exponent");
        Scalar lambda = getParam<Scalar>("SpatialParams.LambdaKr");
        int relPermModel = getParam<int>("SpatialParams.Model");
        Scalar entryP = getParam<Scalar>("SpatialParams.EntryPressure");
        Scalar injectionRate = -getParam<Scalar>("BoundaryConditions.Injectionrate");

        if(correction == 2)//correct the domain height with analytic solution
        {
            charHeight = viscosityNw/viscosityW * aquiferHeight;
        }
        else if(correction == 3)//correct the domain height with numerical solution
        {
            Scalar gasVolume = injectionRate * aquiferHeight
                              * time / (densityN*porosity);
            charHeight = gasVolume/gasPlumeTip;
        }
        if(isnan(charHeight) || isinf(charHeight))
            charHeight = 0.0;

        if(correction == 4)//calculate saturation explicitly
        {
            Scalar timeStepSize = this->timeManager().timeStepSize();
            Scalar mobilityW = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), residualSegSaturation_)/viscosityW;
            Scalar mobilityN = MaterialLaw::krn(this->spatialParams().materialLawParams(dummy_), residualSegSaturation_)/viscosityNw;
//            Scalar drainageFlux = mobilityW*permeability*gravity*(densityW-densityN);
            Scalar drainageFlux = mobilityW*mobilityN/(mobilityW+mobilityN)*permeability*gravity*(densityW-densityN);

            //calculate new residualSegSaturation inside plume with same corrGasPlumeHeight as before (corrGasPlumeHeightOld_) due to vertical flow
            //gas flowing into gasPlume region replaces water out of plume
//            std::cout << "residualSegSaturation vorher " << residualSegSaturation_ << std::endl;

            //neglect tip that flows out of plume (it automatically gets new residualSegSaturation), 1. bug (works perfectly), 2. correct solution (does not work)
            Scalar satDiff = averageSat - averageSatOld_;
//            Scalar satDiff = averageSatOldLocation - averageSatOld_;
//            std::cout << "satDiff " << satDiff << " averageSat " << averageSat << " averageSatOld_ " << averageSatOld_ <<  std::endl;
//            std::cout << "gasPlumeTipOld_ " << gasPlumeTipOld_ << std::endl;
//            std::cout << "gasPlumeTip " << gasPlumeTip << std::endl;
//            if(gasPlumeTipOld_ <= gasPlumeTip + 1e-6 && gasPlumeTipOld_ >= gasPlumeTip - 1e-6)
//            {
                residualSegSaturation_ = residualSegSaturation_ + satDiff*aquiferHeight/corrGasPlumeHeightOld_;
//            }
//            else
//            {
//                residualSegSaturation_ = (residualSegSaturation_ * corrGasPlumeHeightOld_ * porosity * gasPlumeTipOld_ +
//                        (gasPlumeTip - gasPlumeTipOld_) * porosity * corrGasPlumeHeightOld_ - injectionRate/densityN * timeStepSize * aquiferHeight)/
//                        (gasPlumeTip*corrGasPlumeHeightOld_*porosity);
//            }

            //entire new plume region gets new residualSegSaturation depending on corrGasPlumeHeightOld_
//            residualSegSaturation_ = 1.0 - (1.0-residualSegSaturation_) * gasPlumeTipOld_/gasPlumeTip -
//                    (1.0-averageSat)*aquiferHeight/corrGasPlumeHeightOld_ + (1.0-averageSatOld_)*aquiferHeight/corrGasPlumeHeightOld_*gasPlumeTipOld_/gasPlumeTip;
            averageSatOld_ = averageSat;
            if(isinf(residualSegSaturation_) || isnan(residualSegSaturation_))
                residualSegSaturation_ = 1.0;
//            std::cout << "residualSegSaturation nachher1 " << residualSegSaturation_ << std::endl;

            if(residualSegSaturation_ < eps_)
            {
//                std::cout << "attention: time step too large1!!" << std::endl;
                residualSegSaturation_ = 0.0;
                isSegregated_ = true;
            }

            if(this->timeManager().timeStepIndex() < 10)//no segregation in first 10 time steps
            {
                residualSegSaturation_ = 1.0;
                corrGasPlumeHeight_ = aquiferHeight;
            }
            else if(this->timeManager().timeStepIndex() == 10)//start of segregation after 10th time step
            {
                residualSegSaturation_ = averageSat;
                corrGasPlumeHeight_ = aquiferHeight;
                isSegregated_ = false;
            }
            else if(drainageFlux > corrGasPlumeHeightOld_ * residualSegSaturation_ * (1.0-residualSegSaturation_) * porosity/timeStepSize)
            {
//                std::cout << "attention: time step too large2!!" << std::endl;
                residualSegSaturation_ = 0.0;
                isSegregated_ = true;
            }
            else
            {
                //calculate and store new residualSegSaturation
                Scalar oldResidualSegSaturation = residualSegSaturation_;
                residualSegSaturation_ = (corrGasPlumeHeightOld_ * porosity * residualSegSaturation_ - drainageFlux  * timeStepSize/(1.0-residualSegSaturation_)) /
                        (corrGasPlumeHeightOld_ * porosity - drainageFlux * timeStepSize / (1.0-residualSegSaturation_));
//                std::cout << "residualSegSaturation nachher2 " << residualSegSaturation_ << std::endl;
                //calculate and store new corrGasPlumeHeight
                corrGasPlumeHeight_ = corrGasPlumeHeightOld_ - drainageFlux * timeStepSize/(porosity * (1.0-oldResidualSegSaturation));
            }

            if(isSegregated_ == true)
            {
                residualSegSaturation_ = 0.0;
            }

//            corrGasPlumeHeightOld_ = corrGasPlumeHeight;
        }
        else if(relPermModel == 0 || (relPermModel == 1 && exponent == 1))
        {
            residualSegSaturation_ = (charHeight*porosity*viscosityW/(time*permeability*(densityW-densityN)*gravity))*(1.0-resSatW-resSatN) + resSatW;
        }
        else
        {
            if(relPermModel == 2)
            {
                exponent = 2.0/lambda + 3.0;
            }
            residualSegSaturation_ = std::pow((charHeight*porosity*viscosityW/(time*permeability*(densityW-densityN)*gravity)), (1.0/(exponent)))*
                    (1.0-resSatW-resSatN) + resSatW;
        }
    }
    else
    {
        residualSegSaturation_ = this->spatialParams().materialLawParams(dummy_).swr();
        Scalar PseudoResidualSaturation = getParam<Scalar>("VE.PseudoResidualSaturation");
        residualSegSaturation_ = residualSegSaturation_ + PseudoResidualSaturation;
    }
}

/*!
 * \brief The problem name.
 *
 * This is used as a prefix for files generated by the simulation.
 */
const std::string name() const
{
    return name_;
}

bool shouldWriteRestartFile() const
{
    return false;
}

/*!
 * \brief Returns the temperature within the domain.
 *
 * This problem assumes a temperature of 10 degrees Celsius.
 */
Scalar temperatureAtPos(const GlobalPosition& globalPos) const
{
    return 326.0; // -> 53°C
}

// \}

//! Returns the reference pressure for evaluation of constitutive relations
Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
{
    return 1.7e7; // -> 100 bar
}

/*!
* \brief Returns the type of boundary condition.
*
* BC for pressure equation can be dirichlet (pressure) or neumann (flux).
*
* BC for saturation equation can be dirichlet (saturation), neumann (flux), or outflow.
*/
void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
{
    static const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0/2.0;
    static const Scalar length = getParam<std::vector<Scalar>>("Grid.Radial0")[1]+getParam<Scalar>("Grid.WellRadius");
    static const Scalar xCoord = cos(angle)*cos(angle)*length;
    static const Scalar yCoord = sin(angle)*cos(angle)*length;
    if (globalPos[0] > xCoord - eps_ && globalPos[0] < xCoord + eps_
        && globalPos[1] > yCoord - eps_ && globalPos[1] < yCoord + eps_)
    {
        bcTypes.setAllDirichlet();
    }
    // all other boundaries
    else
    {
        bcTypes.setAllNeumann();
    }
}

//! Flag for the type of Dirichlet conditions
/*! The Dirichlet BCs can be specified by a given concentration (mass of
 * a component per total mass inside the control volume) or by means
 * of a saturation.
 *
 * \param bcFormulation The boundary formulation for the conservation equations.
 * \param intersection The intersection on the boundary.
 */
void boundaryFormulation(typename Indices::BoundaryFormulation &bcFormulation, const Intersection& intersection) const
{
    bcFormulation = Indices::concentration;
}

//! Values for dirichlet boundary condition \f$ [Pa] \f$ for pressure and \f$ \frac{mass}{totalmass} \f$ or \f$ S_{\alpha} \f$ for transport.
/*! In case of a dirichlet BC, values for all primary variables have to be set. In the sequential 2p2c model, a pressure
 * is required for the pressure equation and component fractions for the transport equations. Although one BC for the two
 * transport equations can be deduced by the other, it is seperately defined for consistency reasons with other models.
 * Depending on the boundary Formulation, either saturation or total mass fractions can be defined.
 *
 * \param bcValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void dirichletAtPos(PrimaryVariables &bcValues ,const GlobalPosition& globalPos) const
{
    Scalar pRef = referencePressureAtPos(globalPos);
    Scalar temp = temperatureAtPos(globalPos);
    bcValues[Indices::pressureEqIdx] = (pRef + (this->bBoxMax()[dim-1] - globalPos[dim-1])
    * FluidSystem::H2O::liquidDensity(temp, pRef)
    * this->gravity().two_norm());

    bcValues[Indices::contiWEqIdx] = 1.;
    bcValues[Indices::contiNEqIdx] = 1.- bcValues[Indices::contiWEqIdx];
}

/*!
 * \brief Evaluate the boundary conditions for a neumann
 *        boundary segment.
 *
 * \param values The neumann values for the conservation equations [kg / (m^2 *s )]
 * \param intersection The boundary intersection
 *
 * For this method, the \a values parameter stores the mass flux
 * in normal direction of each phase. Negative values mean influx.
 */
void neumann(PrimaryVariables &neumannValues,
             const Intersection &intersection) const
{
    this->setZero(neumannValues);

    const GlobalPosition &globalPos = intersection.geometry().center();
    if (isWell(globalPos))
    {
        static const Scalar cylcesDev = getParam<double>("BoundaryConditions.CyclesDev");
        static const Scalar injectionDurationDev = getParam<double>("BoundaryConditions.InjectionDurationDev");
        static const Scalar idleDurationDev = getParam<double>("BoundaryConditions.IdleDurationDev");
        static const Scalar developmentDuration = cylcesDev*(injectionDurationDev+idleDurationDev);
        static const Scalar wellHeight = getParam<double>("BoundaryConditions.WellHeight");
        static const Scalar wellLength = this->bBoxMax()[dim-1] - wellHeight;
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        if(time <= developmentDuration)
        {
            const int cycleNumber = std::floor(time/(injectionDurationDev+idleDurationDev));
            const Scalar localTimeInCycle = time - cycleNumber*(injectionDurationDev+idleDurationDev);
            if(localTimeInCycle < injectionDurationDev)
                neumannValues[Indices::contiNEqIdx] =
                getParam<double>("BoundaryConditions.InjectionRateDev")*wellLength/this->aquiferHeight();
        }
        else if(this->timeManager().time() + this->timeManager().timeStepSize() > developmentDuration)
        {
            static const Scalar injectionDurationOp = getParam<double>("BoundaryConditions.InjectionDurationOp");
            static const Scalar extractionDurationOp = getParam<double>("BoundaryConditions.ExtractionDurationOp");
            const int cycleNumberOp = std::floor((time-developmentDuration)/(injectionDurationOp+extractionDurationOp));
            const Scalar localTimeInCycle = time-developmentDuration-cycleNumberOp*(injectionDurationOp+extractionDurationOp);
            if(localTimeInCycle < extractionDurationOp)
                neumannValues[Indices::contiNEqIdx] =
                getParam<double>("BoundaryConditions.ExtractionRateOp")*wellLength/this->aquiferHeight();
            else
                neumannValues[Indices::contiNEqIdx] =
                getParam<double>("BoundaryConditions.InjectionRateOp")*wellLength/this->aquiferHeight();
        }
    }
}

//! Source of mass \f$ [\frac{kg}{m^3 \cdot s}] \f$
/*! Evaluate the source term for all phases within a given
 *  volume. The method returns the mass generated (positive) or
 *  annihilated (negative) per volume unit.
 *  Both pressure and transport module regard the neumann-bc values
 *  with the tranport (mass conservation -) equation indices. So the first entry
 *  of the vector is superflous.
 *
 * \param sourceValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void sourceAtPos(PrimaryVariables &sourceValues, const GlobalPosition& globalPos) const
{
    this->setZero(sourceValues);
}

//! Flag for the type of initial conditions
/*! The problem can be initialized by a given concentration (mass of
 * first component per total mass inside the control volume) or by means
 * of a saturation (of first phase). VE models: choose concentration.
 */
void initialFormulation(typename Indices::BoundaryFormulation &initialFormulation, const Element& element) const
{
    initialFormulation = Indices::concentration;
}

//! Concentration initial condition (dimensionless)
/*! Full VE models: gives back local gasPlumeDist!
 */
Scalar initConcentrationAtPos(const GlobalPosition& globalPos) const
{
    const Scalar gasPlumeDistGlobal = getParam<Scalar>("Initial.gasPlumeDistGlobal");
    const Scalar localTransform = globalPos[dim-1] - this->aquiferHeight()/2.0;
    return gasPlumeDistGlobal-localTransform;
}

const bool isWell(const GlobalPosition& globalPos) const
{
    const Scalar angle = M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])/180.0/2.0;
    const Scalar xCoord = cos(angle)*cos(angle)*getParam<Scalar>("Grid.WellRadius");
    const Scalar yCoord = sin(angle)*cos(angle)*getParam<Scalar>("Grid.WellRadius");
    if (globalPos[0] > xCoord - eps_ && globalPos[0] < xCoord + eps_
        && globalPos[1] > yCoord - eps_ && globalPos[1] < yCoord + eps_)
        return 1;
    else
        return 0;
}

Scalar residualSegSaturation() const
{
    return residualSegSaturation_;
}

Scalar residualSegSatAverage() const
{
    return residualSegSatAverage_;
}

Scalar averageSat() const
{
    return averageSat_;
}

Scalar corrGasPlumeHeightAverage() const
{
    return corrGasPlumeHeight_;
}

Scalar capillaryTransitionZone() const
{
    return CTZ_;
}

const Scalar aquiferHeight() const
{
    return aquiferHeight_;
}

const Scalar aquiferLength() const
{
    return aquiferLength_;
}

const CellArray numberOfCells() const
{
    if (hasParam("Grid.Cells"))
        return getParam<CellArray>("Grid.Cells");
    else if (hasParam("Grid.Cells" +  std::to_string(0)))
    {
        CellArray numberOfCells;
        for (int i = 0; i < dim; ++i)
        {
            numberOfCells[i] = getParam<Scalar>("Grid.Cells" +  std::to_string(i));
        }
        return numberOfCells;
    }
    else
        DUNE_THROW(Dune::GridError, "Unknown grid specification. Number of cells could not be identified.");
}


private:

const Scalar eps_;
Dumux::VtkMultiWriter<LeafGridView> visualizationWriter_;
Dumux::VtkMultiWriter<LeafGridView> normWriter_;
std::string name_;
std::map<int, Element> mapColumns_;
std::ofstream outputFile_;
Element dummy_;
Scalar CTZ_, aquiferHeight_, aquiferLength_;
Scalar residualSegSaturation_;
Scalar residualSegSatAverage_;
Scalar corrGasPlumeHeight_;
Scalar corrGasPlumeHeightOld_;
Scalar gasPlumeTipOld_;
Scalar averageSatOld_;
Scalar averageSat_;
bool isSegregated_;
Dumux::GnuplotInterface<double> gnuplot_;
};
} //end namespace

#endif
