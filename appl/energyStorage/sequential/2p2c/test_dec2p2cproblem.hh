// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SequentialTwoPTwoCTests
 * \brief test problem for the sequential 2p2c model
 */
#ifndef DUMUX_TEST_2P2C_PROBLEM_HH
#define DUMUX_TEST_2P2C_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dumux/porousmediumflow/2p2c/sequential/problem.hh>
#include <dumux/porousmediumflow/2p2c/sequential/fvpressure.hh>
#include <dumux/porousmediumflow/2p2c/sequential/fvtransport.hh>

// fluid properties
#include <dumux/material/fluidsystems/h2och4.hh>
#include <dumux/material/fluidsystems/h2oh2.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include "test_dec2p2c_spatialparams.hh"

using namespace std;
namespace Dumux
{
template<class TypeTag>
class TestDecTwoPTwoCProblem;

//////////
// Specify the properties
//////////
namespace Properties
{
// Create new type tags
namespace TTag {
    struct TestDecTwoPTwoC { using InheritsFrom = std::tuple<Test2P2CSpatialParams, SequentialTwoPTwoC>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TestDecTwoPTwoC> { using type = Dune::UGGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TestDecTwoPTwoC> { using type = TestDecTwoPTwoCProblem<TypeTag>; };

//Set the transport model
template<class TypeTag>
struct TransportModel<TypeTag, TTag::TestDecTwoPTwoC> { using type = FVTransport2P2C<TypeTag>; };

//Set the pressure model
template<class TypeTag>
struct PressureModel<TypeTag, TTag::TestDecTwoPTwoC> { using type = FVPressure2P2C<TypeTag>; };

// Select fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TestDecTwoPTwoC>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::H2OCH4<Scalar>;
};

template<class TypeTag>
struct PressureFormulation<TypeTag, TTag::TestDecTwoPTwoC> { static constexpr int value = GetPropType<TypeTag, Properties::Indices>::pressureW; };

template<class TypeTag>
struct EnableCapillarity<TypeTag, TTag::TestDecTwoPTwoC> { static constexpr bool value = true; };
template<class TypeTag>
struct BoundaryMobility<TypeTag, TTag::TestDecTwoPTwoC> { static constexpr int value = GetPropType<TypeTag, Properties::Indices>::satDependent; };

template<class TypeTag>
struct LinearSolver<TypeTag, TTag::TestDecTwoPTwoC> { using type = SuperLUBackend; };
}

/*!
 * \ingroup IMPETtests
 *
 * \brief test problem for the sequential 2p2c model
 *
 * The domain is box shaped (3D). All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet). A Gas (Nitrogen)
 * is injected over a vertical well in the center of the domain.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_dec2p2c -parameterFile ./test_dec2p2c.input</tt>
 */
template<class TypeTag>
class TestDecTwoPTwoCProblem: public IMPETProblem2P2C<TypeTag>
{
using ParentType = IMPETProblem2P2C<TypeTag>;
using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
using Grid = typename GridView::Grid;
using TimeManager = GetPropType<TypeTag, Properties::TimeManager>;
using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
using WettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::H2O;
using NonWettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::CH4;

using CellData = GetPropType<TypeTag, Properties::CellData>;
using BoundaryTypes = GetPropType<TypeTag, Properties::SequentialBoundaryTypes>;
using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
using MaterialLaw = GetPropType<TypeTag, Properties::MaterialLaw>;

enum
{
    dim = GridView::dimension, dimWorld = GridView::dimensionworld
};

enum
{
    wPhaseIdx = Indices::wPhaseIdx,
    nPhaseIdx = Indices::nPhaseIdx,
    wCompIdx = Indices::wCompIdx,
    nCompIdx = Indices::nCompIdx
};

enum
{
    numPhases = FluidSystem::numPhases
};

enum VEModel
{
    sharpInterface,
    capillaryFringe
};

using Scalar = GetPropType<TypeTag, Properties::Scalar>;

using Element = typename GridView::Traits::template Codim<0>::Entity;
using Intersection = typename GridView::Intersection;
using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

using CellArray = std::array<unsigned int, dimWorld>;
using PhaseVector = Dune::FieldVector<Scalar, numPhases>;
using FluidState = GetPropType<TypeTag, Properties::FluidState>;

public:
TestDecTwoPTwoCProblem(TimeManager& timeManager, Grid& grid) :
ParentType(timeManager, grid)
{
    // initialize the tables of the fluid system
    FluidSystem::init(326.0-10, 326.0+10, 10, 0.5e6, 5e7, 200);

    int outputInterval = 0;
    if (hasParam("Problem.OutputInterval"))
    {
        outputInterval = getParam<int>("Problem.OutputInterval");
    }
    this->setOutputInterval(outputInterval);

    Scalar outputTimeInterval = 1e6;
    if (hasParam("Problem.OutputTimeInterval"))
    {
        outputTimeInterval = getParam<Scalar>("Problem.OutputTimeInterval");
    }
    this->setOutputTimeInterval(outputTimeInterval);

    name_ = getParam<std::string>("Problem.Name");

    // store pointer to all elements in a multimap, elements belonging to the same column
    // have the same key, starting with key = 1 for first column
    // TODO: only works for equidistant grids
    int columnIndex = 0;
    for (const auto& element : Dune::elements(this->gridView()))
    {
        // identify column number
        GlobalPosition globalPos = element.geometry().center();
        CellArray numberOfCellsX = getParam<CellArray>("Grid.Cells");
        double deltaX = this->bBoxMax()[0]/numberOfCellsX[0];

        columnIndex = round((globalPos[0] + (deltaX/2.0))/deltaX);

        mapColumns_.insert(std::make_pair(columnIndex, element));
        dummy_ = element;
    }

    //calculate segregation time
    Scalar height = this->bBoxMax()[dim-1];
    Scalar porosity = this->spatialParams().porosity(dummy_);
    GlobalPosition globalPos = dummy_.geometry().center();
    Scalar pRef = referencePressureAtPos(globalPos);
    Scalar tempRef = temperatureAtPos(globalPos);
    viscosityW_ = WettingPhase::liquidViscosity(tempRef, pRef);
    Scalar permeability = this->spatialParams().intrinsicPermeability(dummy_)[dim-1][dim-1];
    Scalar gravity = this->gravity().two_norm();
    Scalar densityW = WettingPhase::liquidDensity(tempRef, pRef);
    Scalar densityN = NonWettingPhase::gasDensity(tempRef, pRef);
    segTime_ = (height*porosity*viscosityW_)/(permeability*gravity*(densityW-densityN));
    std::cout << "segTime " << segTime_ << std::endl;

    veModel_ = getParam<int>("VE.VEModel");

    //reset all output
    system("rm *.out");
}

/*!
 * \name Problem parameters
 */
// \{

//! The problem name.
/*! This is used as a prefix for files generated by the simulation.
*/
std::string name() const
{
    return name_;
}
//!  Returns true if a restart file should be written.
/* The default behaviour is to write no restart file.
 */
bool shouldWriteRestartFile() const
{
    return false;
}

//! Returns the temperature within the domain.
/*! This problem assumes a temperature of 10 degrees Celsius.
 * \param globalPos The global Position
 */
Scalar temperatureAtPos(const GlobalPosition& globalPos) const
{
    return 326.0;
}

// \}
//! Returns the reference pressure.
 /*This pressure is used in order to calculate the material properties
 * at the beginning of the initialization routine. It should lie within
 * a reasonable pressure range for the current problem.
 * \param globalPos The global Position
 */
Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
{
    return 1.0e7;
}
//! Type of boundary condition.
/*! Defines the type the boundary condition for the conservation equation,
 *  e.g. Dirichlet or Neumann. The Pressure equation is acessed via
 *  Indices::pressureEqIdx, while the transport (or mass conservation -)
 *  equations are reached with Indices::contiWEqIdx and Indices::contiNEqIdx
 *
 * \param bcTypes The boundary types for the conservation equations
 * \param globalPos The global Position
 */
void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
{
    if (globalPos[0] > this->bBoxMax()[0] - eps_)
    {
        bcTypes.setAllDirichlet();
    }
    // all other boundaries
    else
    {
        bcTypes.setAllNeumann();
    }
}

//! Flag for the type of Dirichlet conditions
/*! The Dirichlet BCs can be specified by a given concentration (mass of
 * a component per total mass inside the control volume) or by means
 * of a saturation.
 *
 * \param bcFormulation The boundary formulation for the conservation equations.
 * \param intersection The intersection on the boundary.
 */
void boundaryFormulation(typename Indices::BoundaryFormulation &bcFormulation, const Intersection& intersection) const
{
    bcFormulation = Indices::saturation;
}
//! Values for dirichlet boundary condition \f$ [Pa] \f$ for pressure and \f$ \frac{mass}{totalmass} \f$ or \f$ S_{\alpha} \f$ for transport.
/*! In case of a dirichlet BC, values for all primary variables have to be set. In the sequential 2p2c model, a pressure
 * is required for the pressure equation and component fractions for the transport equations. Although one BC for the two
 * transport equations can be deduced by the other, it is seperately defined for consistency reasons with other models.
 * Depending on the boundary Formulation, either saturation or total mass fractions can be defined.
 *
 * \param bcValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void dirichletAtPos(PrimaryVariables &bcValues ,const GlobalPosition& globalPos) const
{
    bcValues = 0;

    if (globalPos[0] > this->bBoxMax()[0] - eps_)
    {
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar temp = temperatureAtPos(globalPos);
        bcValues[Indices::pressureEqIdx] = (pRef + (this->bBoxMax()[dim-1] - globalPos[dim-1])
        * FluidSystem::H2O::liquidDensity(temp, pRef)
        * this->gravity().two_norm());

        bcValues[Indices::contiWEqIdx] = 1.;
        bcValues[Indices::contiNEqIdx] = 1.- bcValues[Indices::contiWEqIdx];
    }
}

//! Value for neumann boundary condition \f$ [\frac{kg}{m^3 \cdot s}] \f$.
/*! In case of a neumann boundary condition, the flux of matter
 *  is returned. Both pressure and transport module regard the neumann-bc values
 *  with the tranport (mass conservation -) equation indices. So the first entry
 *  of the vector is superflous.
 *  An influx into the domain has negative sign.
 *
 * \param neumannValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void neumannAtPos(PrimaryVariables &neumannValues, const GlobalPosition& globalPos) const
{
    this->setZero(neumannValues);
    if (globalPos[0] < eps_)
    {
        neumannValues[Indices::contiNEqIdx] = getParam<double>("BoundaryConditions.Injectionrate");
    }
}
//! Source of mass \f$ [\frac{kg}{m^3 \cdot s}] \f$
/*! Evaluate the source term for all phases within a given
 *  volume. The method returns the mass generated (positive) or
 *  annihilated (negative) per volume unit.
 *  Both pressure and transport module regard the neumann-bc values
 *  with the tranport (mass conservation -) equation indices. So the first entry
 *  of the vector is superflous.
 *
 * \param sourceValues Vector holding values for all equations (pressure and transport).
 * \param globalPos The global Position
 */
void sourceAtPos(PrimaryVariables &sourceValues, const GlobalPosition& globalPos) const
{
    this->setZero(sourceValues);
}
//! Flag for the type of initial conditions
/*! The problem can be initialized by a given concentration (mass of
 * a component per total mass inside the control volume) or by means
 * of a saturation.
 */
void initialFormulation(typename Indices::BoundaryFormulation &initialFormulation, const Element& element) const
{
    initialFormulation = Indices::concentration;
}
//! Sat initial condition (dimensionless)
/*! The problem is initialized with the following saturation.
 */
Scalar initConcentrationAtPos(const GlobalPosition& globalPos) const
{
    return 1;
}

void postTimeStep()
{
    ParentType::postTimeStep();

    if (getParam<bool>("Output.PlotCriteria"))
    {
        const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
        static std::vector<Scalar> minGasPlumeDist(numberOfCells[0], this->bBoxMax()[dim - 1]);
        const int time = this->timeManager().time() + this->timeManager().timeStepSize();
        //iterate over grid columns
        for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
        {
            //iterate over all cells in one grid column,
            //calculate averageConcColumn, collect sat and relPerm
            Scalar volumeColumn = 0.0;
            Scalar averageTotalConcColumn = 0.0;
            PhaseVector pRefColumn;// reference pressures in column
            pRefColumn[wPhaseIdx] = 0.0;
            pRefColumn[nPhaseIdx] = 1e100;
            Scalar columnMinPressureW = 1e100;
            typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
            for (; it != mapColumns_.upper_bound(columnIndex); ++it)
            {
                const GlobalPosition globalPos = (it->second).geometry().center();
                const Scalar volume = (it->second).geometry().volume();
                const int globalIdxI = this->variables().index(it->second);
                const CellData& cellData = this->variables().cellData(globalIdxI);

                Scalar positionZ = globalPos[dim - 1];
                Scalar satW = cellData.saturation(wPhaseIdx);
                Scalar relPermW = cellData.mobility(wPhaseIdx) * cellData.viscosity(wPhaseIdx);

                //only plot profiles for specific times and columns
                if ((columnIndex == 1 && (this->timeManager().episodeWillBeFinished()  || (time > 343628 && time < 415442) || this->timeManager().willBeFinished()))
                    || (columnIndex == 20 && (this->timeManager().episodeWillBeFinished() || this->timeManager().willBeFinished()))
                    || (columnIndex == 100 && (this->timeManager().episodeWillBeFinished() || this->timeManager().willBeFinished())))
                {
                    //write data to sat-plots
                    std::ostringstream oss;
                    oss << "satProfile_" << columnIndex << "-" << time << ".out";
                    std::string fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ << " " << satW << std::endl;
                    outputFile_.close();

                    //write data to relPerm-plots
                    oss.str("");
                    oss << "relPermProfile_" << columnIndex << "-" << time << ".out";
                    fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ << " " << relPermW << std::endl;
                    outputFile_.close();
                }

                volumeColumn += volume;
                averageTotalConcColumn += cellData.totalConcentration(nCompIdx) * volume;

                if((it->second).geometry().center()[dim-1] > minGasPlumeDist[columnIndex])
                    pRefColumn[wPhaseIdx] = std::max(pRefColumn[wPhaseIdx], cellData.pressure(wPhaseIdx));
                pRefColumn[nPhaseIdx] = std::min(pRefColumn[nPhaseIdx], cellData.pressure(nPhaseIdx));
                columnMinPressureW = std::min(columnMinPressureW, cellData.pressure(wPhaseIdx));
            }
            averageTotalConcColumn /= volumeColumn;
            if(pRefColumn[wPhaseIdx] < 1e-8)
                pRefColumn[wPhaseIdx] = columnMinPressureW;

            const std::vector<Scalar> distances = calculateGasPlumeDist(averageTotalConcColumn, pRefColumn, minGasPlumeDist[columnIndex]);
            const Scalar gasPlumeDist = distances[0];
            minGasPlumeDist[columnIndex] = distances[1];

            //only plot profiles for specific times and columns
            if ((columnIndex == 1 && (this->timeManager().episodeWillBeFinished() || (time > 343628 && time < 415442) || this->timeManager().willBeFinished()))
                || (columnIndex == 20 && (this->timeManager().episodeWillBeFinished() || this->timeManager().willBeFinished()))
                || (columnIndex == 100 && (this->timeManager().episodeWillBeFinished() || this->timeManager().willBeFinished())))
            {
                //collect data for sat and relPerm VE profiles
                static const int steps = 100;
                static const Scalar domainHeight = this->bBoxMax()[dim - 1];
                static const Scalar deltaZ = domainHeight/steps;
                for (int i = 0; i <= steps; i++)
                {
                    Scalar positionZ2 = i*deltaZ;
                    Scalar reconstSatW = reconstSaturation(positionZ2, gasPlumeDist, minGasPlumeDist[columnIndex], pRefColumn);
                    Scalar reconstRelPermW = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), reconstSatW);

                    //write data to reconstSat-plots
                    std::ostringstream oss;
                    oss << "reconstSatProfile_" << columnIndex << "-" << time << ".out";
                    std::string fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ2 << " " << reconstSatW << std::endl;
                    outputFile_.close();

                    //write data to reconstRelPerm-plots
                    oss.str("");
                    oss << "reconstRelPermProfile_" << columnIndex << "-" << time << ".out";
                    fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionZ2 << " " << reconstRelPermW << std::endl;
                    outputFile_.close();
                }
            }

            //iterate over all cells in one grid column and calculate VE criteria
            Scalar satCrit = 0;
            Scalar relPermCrit = 0;
            Scalar positionX = 0;
            static const Scalar domainHeight = this->bBoxMax()[dim - 1];
            static const Scalar deltaZ = domainHeight/numberOfCells[dim-1];
            it = mapColumns_.lower_bound(columnIndex);
            for (; it != mapColumns_.upper_bound(columnIndex); ++it)
            {
                const GlobalPosition globalPos = (it->second).geometry().center();
                positionX = globalPos[0];
                const Scalar lowerBound = globalPos[dim-1] - deltaZ/2.0;
                const Scalar upperBound = globalPos[dim-1] + deltaZ/2.0;
                const int globalIdxI = this->variables().index(it->second);
                const Scalar saturationW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
                satCrit += calculateSatCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist, minGasPlumeDist[columnIndex], pRefColumn);
                relPermCrit += calculateKrwCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist, minGasPlumeDist[columnIndex], pRefColumn);
            }

            satCrit /= (domainHeight-gasPlumeDist);
            relPermCrit /= (domainHeight-gasPlumeDist);

            if(gasPlumeDist > domainHeight - 1e-3)
            {
                satCrit = 0;
                relPermCrit = 0;
            }

            //only plot VE criteria over time for specific columns
            if (columnIndex == 1 || columnIndex == 20 || columnIndex == 100)
            {
                //write data to sat-clotsriterion p
                std::ostringstream oss;
                oss << "satCritColumn_" << columnIndex << ".out";
                std::string fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << time/segTime_ << " " << satCrit << std::endl;
                outputFile_.close();

                //write data to relPerm-criterion plots
                oss.str("");
                oss << "relPermCritColumn_" << columnIndex << ".out";
                fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << time/segTime_ << " " << relPermCrit << std::endl;
                outputFile_.close();
            }

            //only plot VE criteria over domain for specific times
            if (this->timeManager().episodeWillBeFinished() || this->timeManager().willBeFinished())
            {
                //write data to satCrit-plots
                std::ostringstream oss;
                oss << "satCrit_" << time << ".out";
                std::string fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << positionX << " " << satCrit << std::endl;
                outputFile_.close();

                //write data to relPermCrit-plots
                oss.str("");
                oss << "relPermCrit_" << time << ".out";
                fileName = oss.str();
                outputFile_.open(fileName, std::ios::app);
                outputFile_ << positionX << " " << relPermCrit << std::endl;
                outputFile_.close();
            }
        }

    }

    if (getParam<bool>("Output.PlotDissolvedMass"))
    {
        bool plotCriteria = false;
        const int timeStepIdx = this->timeManager().timeStepIndex();
        //only plot VE criteria over domain for specific timeStepIdx
        if (timeStepIdx % 10 == 0)
        {
            plotCriteria = true;
        }

        if(plotCriteria)
        {
            Scalar totalMassGas = 0.0;
            Scalar totalMassDissolvedGas = 0.0;
            Scalar totalMassDissolvedGasInPlume = 0.0;
            const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
            //iterate over grid columns
            for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
            {
                //iterate over all cells in one grid column and calculate mass of dissolved gas
                typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                {
                    const Scalar volume = (it->second).geometry().volume();
                    const int eIdxGlobal = this->variables().index(it->second);
                    CellData& cellData = this->variables().cellData(eIdxGlobal);

                    totalMassGas += volume * cellData.totalConcentration(nCompIdx);
                    totalMassDissolvedGas += cellData.saturation(wPhaseIdx) * volume
                    * this->spatialParams().porosity(it->second) * cellData.density(wPhaseIdx)
                    * cellData.massFraction(wPhaseIdx, nCompIdx);

                    //identify gas plume cells
                    if (cellData.saturation(wPhaseIdx) < 1.0 - eps_)
                    {
                        totalMassDissolvedGasInPlume += cellData.saturation(wPhaseIdx) * volume
                        * this->spatialParams().porosity(it->second) * cellData.density(wPhaseIdx)
                        * cellData.massFraction(wPhaseIdx, nCompIdx);
                    }
                }
            }

            //write data to mass-plots
            std::ostringstream oss;
            oss << "massDissolvedGas" << ".out";
            std::string fileName = oss.str();
            outputFile_.open(fileName, std::ios::app);
            outputFile_ << this->timeManager().time() + this->timeManager().timeStepSize()
                        << " " << totalMassDissolvedGas <<  " " <<  totalMassDissolvedGas/totalMassGas*100
                        << " " <<  totalMassDissolvedGasInPlume << " " << totalMassDissolvedGasInPlume/totalMassDissolvedGas*100 << std::endl;
            outputFile_.close();
        }
    }


    if (getParam<bool>("Output.CheckMassConservativity"))
    {
        //check mass conservativity:
        Scalar totalMassW = 0.0;
        Scalar totalMassN = 0.0;
        Scalar boundaryFluxW = 0.0;
        Scalar boundaryFluxN = 0.0;
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar tempRef = temperatureAtPos(globalPos);
        Scalar porosity = this->spatialParams().porosity(dummy_);
        Scalar satW = 1.0;//Attention: change according to initial conditions
        static Scalar totalMassWExpected = satW * this->bBoxMax()[0] * this->bBoxMax()[dim - 1]
        * porosity * WettingPhase::liquidDensity(tempRef, pRef);
        static Scalar totalMassNExpected = (1.0-satW) * this->bBoxMax()[0] * this->bBoxMax()[dim - 1]
        * porosity * NonWettingPhase::gasDensity(tempRef, pRef);
        for (const auto& element : elements(this->gridView()))
        {
            int eIdxGlobal = this->variables().index(element);
            GlobalPosition globalPos = element.geometry().center();
            CellData& cellData = this->variables().cellData(eIdxGlobal);
            Scalar pRef = referencePressureAtPos(globalPos);
            Scalar temp = temperatureAtPos(globalPos);
            Scalar massW = cellData.saturation(wPhaseIdx) * element.geometry().volume()
            * this->spatialParams().porosity(element) * WettingPhase::liquidDensity(temp, pRef);
            Scalar massN = cellData.saturation(nPhaseIdx) * element.geometry().volume()
            * this->spatialParams().porosity(element) * NonWettingPhase::gasDensity(temp, pRef);
            totalMassW += massW;
            totalMassN += massN;

            // calculate flow of gas over boundary
            for (const auto& intersection : intersections(this->gridView(), element))
            {
                if (intersection.boundary())
                {
                    int isIndex = intersection.indexInInside();
                    GlobalPosition unitOuterNormal = intersection.centerUnitOuterNormal();
                    Scalar faceArea = intersection.geometry().volume();

                    boundaryFluxW -= cellData.fluxData().velocity(wPhaseIdx, isIndex)
                    * unitOuterNormal * faceArea * WettingPhase::liquidDensity(temp, pRef);
                    boundaryFluxN -= cellData.fluxData().velocity(nPhaseIdx, isIndex)
                    * unitOuterNormal * faceArea * NonWettingPhase::gasDensity(temp, pRef);

                    BoundaryTypes bcTypes;
                    this->boundaryTypes(bcTypes, intersection);
                }
            }
        }
        Scalar timeStepSize = this->timeManager().timeStepSize();
        totalMassWExpected += boundaryFluxW * timeStepSize;
        totalMassNExpected += boundaryFluxN * timeStepSize;
        std::cout << "Error in mass W: " << totalMassWExpected - totalMassW << ". ";
        std::cout << "Error in mass N: " << totalMassNExpected - totalMassN << ". ";
    }
}

/*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
 *
 * Stores minGasPlumeDist for all grid cells
 */
const std::vector<Scalar> calculateGasPlumeDist(const Scalar totalGasConcentration, const PhaseVector columnRefPressures, Scalar minGasPlumeDist) const
{
    const int veModel = getParam<int>("VE.VEModel");
    const Scalar domainHeight = this->bBoxMax()[dim - 1];
    const Scalar gravity = this->gravity().two_norm();

    const Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
    const Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
    const Scalar porosity = this->spatialParams().porosity(dummy_);
    const Scalar entryP = this->spatialParams().materialLawParams(dummy_).pe();
    const Scalar lambda = this->spatialParams().materialLawParams(dummy_).lambda();

    const FluidState& fluidStatePlume = this->fluidStatePlume(dummy_, columnRefPressures);
    const Scalar X_l_gComp = fluidStatePlume.massFraction(wPhaseIdx, nCompIdx);
    const Scalar X_g_gComp = fluidStatePlume.massFraction(nPhaseIdx, nCompIdx);
    const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
    const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

    if(minGasPlumeDist > domainHeight)
        minGasPlumeDist = domainHeight;

    Scalar gasPlumeDist = 0.0;

    if(totalGasConcentration < 1e-12)
        gasPlumeDist = domainHeight;
    else if (veModel == sharpInterface) //calculate gasPlumeDist for sharp interface ve model
    {
        const Scalar minGasPlumeIntegral = (domainHeight - minGasPlumeDist) *
        (porosity * (1-resSatW) * densityN * X_g_gComp + porosity * resSatW * densityW * X_l_gComp);
        //calculate gasPlumeDist for gasPlumeDist > minGasPlumeDist
        if (totalGasConcentration * domainHeight < minGasPlumeIntegral - 1e-6)
        {
            gasPlumeDist = (totalGasConcentration * domainHeight + porosity * densityN * X_g_gComp *
            (minGasPlumeDist * resSatN - domainHeight * (1-resSatW))
            + porosity * densityW * X_l_gComp * (minGasPlumeDist * (1-resSatN) - domainHeight * resSatW)) /
            (porosity * (1-resSatW-resSatN) * (densityW * X_l_gComp - densityN * X_g_gComp));
        }
        //calculate gasPlumeDist for gasPlumeDist < minGasPlumeDist and calculate new minGasPlumeDist
        else if (totalGasConcentration * domainHeight > minGasPlumeIntegral + 1e-6)
        {
            gasPlumeDist = domainHeight - (domainHeight * totalGasConcentration /
            ((porosity * (1-resSatW) * densityN * X_g_gComp) + (porosity * resSatW * densityW * X_l_gComp)));
            minGasPlumeDist = gasPlumeDist;
        }
        //calculate gasPlumeDist for gasPlumeDist = minGasPlumeDist
        else
        {
            gasPlumeDist = minGasPlumeDist;
        }
    }

    else if (veModel == capillaryFringe) //calculate gasPlumeDist for capillary fringe model
    {
        const Scalar minGasPlumeIntegral =
        porosity * densityN * X_g_gComp * (domainHeight - minGasPlumeDist)
        + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
        (1.0 / (1.0 - lambda) * std::pow(((domainHeight - minGasPlumeDist) * (densityW - densityN) * gravity
        + entryP),(1.0 - lambda))
        * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
        + resSatW * (domainHeight - minGasPlumeDist)
        - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity));

        const Scalar residualIntegral = (domainHeight - minGasPlumeDist) * porosity * (resSatN * densityN * X_g_gComp
        + (1 - resSatN) * densityW * X_l_gComp);

        Scalar residual = 1.0;
        // calculate gasPlumeDist for gasPlumeDist > minGasPlumeDist
        // check if gas concentration is high enough to sustain minGasPlumeDist
        if (totalGasConcentration * domainHeight < minGasPlumeIntegral - 1e-6
            && totalGasConcentration * domainHeight > residualIntegral)
        {
            //GasPlumeDist>0 (always in this case)
            gasPlumeDist = minGasPlumeDist; //XiStart
            //solve equation for
            int count = 0;
            for (;count < 100; count++)
            {
                residual =
                porosity * densityN * X_g_gComp * (domainHeight - gasPlumeDist)
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity
                + entryP),(1.0 - lambda))
                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                + resSatW * (domainHeight - gasPlumeDist)
                - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))
                + (gasPlumeDist - minGasPlumeDist) * porosity *
                (resSatN * densityN * X_g_gComp + (1 - resSatN) * densityW * X_l_gComp)
                - totalGasConcentration * domainHeight;

                if (fabs(residual) < 1e-10)
                {
                    break;
                }

                const Scalar derivation =
                - porosity * densityN * X_g_gComp
                + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                (std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                * (resSatN + resSatW - 1.0) * std::pow(entryP, lambda) - resSatW)
                + porosity * (resSatN * densityN * X_g_gComp + (1 - resSatN) * densityW * X_l_gComp);

                gasPlumeDist = gasPlumeDist - residual / (derivation);

                if (count == 99)
                    std::cout << std::setprecision(15) << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                    << "Iteration crashed in following routine: "
                    << "gasPlumeDist > minGasPlumeDist, with: "
                    << "totalGasConcentration = " << totalGasConcentration
                    << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                    << ", residual = " << residual
                    << ", derivation = " << derivation
                    << ", minGasPlumeDist = " << minGasPlumeDist
                    << " and gasPlumeDist: " << gasPlumeDist << "."
                    << std::endl;
            }
        }
        // calculate gasPlumeDist for gasPlumeDist < minGasPlumeDist (or unphysical minGasPlumeDist)
        // and calculate new minGasPlumeDist
        else if (totalGasConcentration * domainHeight > minGasPlumeIntegral + 1e-6
            || (totalGasConcentration * domainHeight < minGasPlumeIntegral - 1e-6 &&
            totalGasConcentration * domainHeight < residualIntegral))
        {
            //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
            const Scalar fullIntegral =
            porosity * densityN * X_g_gComp * domainHeight
            + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
            (1.0 / (1.0 - lambda) * std::pow((domainHeight * (densityW - densityN) * gravity +
            entryP),(1.0 - lambda))
            * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
            + resSatW * domainHeight
            - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity));
            //GasPlumeDist>0
            if (totalGasConcentration * domainHeight < fullIntegral - 1e-6)
            {
                gasPlumeDist = minGasPlumeDist; //XiStart
                //solve equation for
                int count = 0;
                for (; count < 100; count++)
                {
                    residual =
                    porosity * densityN * X_g_gComp * (domainHeight - gasPlumeDist)
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity +
                    entryP),(1.0 - lambda))
                    * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                    + resSatW * (domainHeight - gasPlumeDist)
                    - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))
                    - totalGasConcentration * domainHeight;

                    if (fabs(residual) < 1e-12)
                    {
                        break;
                    }

                    const Scalar derivation =
                    - porosity * densityN * X_g_gComp
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                    * (- 1.0 + resSatW + resSatN) * std::pow(entryP, lambda) - resSatW);

                    gasPlumeDist = gasPlumeDist - residual / (derivation);

                    if (count == 99)
                        std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                        << "Iteration crashed in following routine: "
                        << "gasPlumeDist < minGasPlumeDist, GasPlumeDist > 0, with: "
                        << "totalGasConcentration = " << totalGasConcentration
                        << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                        << ", fullIntegral = " << fullIntegral
                        << ", residual = " << residual
                        << ", derivation = " << derivation
                        << ", minGasPlumeDist = " << minGasPlumeDist
                        << " and gasPlumeDist: " << gasPlumeDist << "."
                        << std::endl;
                }
            }
            //GasPlumeDist<0
            else if (totalGasConcentration * domainHeight > fullIntegral + 1e-6)
            {
                gasPlumeDist = 0.0; //XiStart
                //solve equation
                int count = 0;
                for (; count < 100; count++)
                {
                    residual =
                    porosity * densityN * X_g_gComp * domainHeight
                    + porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    (1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), (1.0 - lambda))
                    * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                    + resSatW * domainHeight
                    - 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDist * (densityW - densityN)
                    * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                    / ((densityW - densityN) * gravity))
                    - totalGasConcentration * domainHeight;

                    if (fabs(residual) < 1e-10)
                    {
                        break;
                    }

                    const Scalar derivation =
                    porosity * (densityW * X_l_gComp - densityN * X_g_gComp) *
                    std::pow(((domainHeight - gasPlumeDist) * (densityW - densityN) * gravity + entryP), -lambda)
                    * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda)
                    + std::pow((entryP - gasPlumeDist * (densityW - densityN) * gravity), -lambda)
                    * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);

                    gasPlumeDist = gasPlumeDist - residual / (derivation);

                    if (count == 99)
                        std::cout << "Iterating gasPlumeDist not successfull, count is at maximum, exit iteration. "
                        << "Iteration crashed in following routine: "
                        << "gasPlumeDist < minGasPlumeDist, GasPlumeDist < 0, with: "
                        << "totalGasConcentration = " << totalGasConcentration
                        << ", minGasPlumeIntegral = " << minGasPlumeIntegral
                        << ", fullIntegral = " << fullIntegral
                        << ", residual = " << residual
                        << ", derivation = " << derivation
                        << ", minGasPlumeDist = " << minGasPlumeDist
                        << " and gasPlumeDist: " << gasPlumeDist << "."
                        << std::endl;
                }
            }
            //GasPlumeDist=0
            else
            {
                gasPlumeDist = 0.0;
            }
            minGasPlumeDist = std::max(0.0, gasPlumeDist);
        }
        //calculate gasPlumeDist for gasPlumeDist = minGasPlumeDist
        else
        {
            gasPlumeDist = minGasPlumeDist;
        }
    }
    std::vector<Scalar> distances(2);
    distances[0] = gasPlumeDist;
    distances[1] = minGasPlumeDist;
    return distances;
}

/*! \brief Determines fluid state inside plume
 */
FluidState fluidStatePlume(const Element& element, const PhaseVector columnRefPressures) const
{
    const GlobalPosition& globalPos = element.geometry().center();

    const Scalar resSatW = this->spatialParams().materialLawParams(element).swr();
    const Scalar porosity = this->spatialParams().porosity(element);
    const Scalar temperature = this->temperatureAtPos(globalPos);

    FluidState fluidStatePlume;
    CompositionalFlash<Scalar, FluidSystem> flashSolver;
    flashSolver.saturationFlash2p2c(fluidStatePlume,
                                    resSatW,
                                    columnRefPressures,
                                    porosity,
                                    temperature);

    return fluidStatePlume;
}

/*! \brief Calculates gasPlumeDist, distance of gas plume from bottom
 *
 * Stores minGasPlumeDist for all grid cells
 */
const Scalar reconstSaturation(const Scalar height,
                               const Scalar gasPlumeDist,
                               const Scalar minGasPlumeDist,
                               const PhaseVector columnRefPressures) const
{
    const Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
    const Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
    const Scalar entryP = this->spatialParams().materialLawParams(dummy_).pe();
    const Scalar lambda = this->spatialParams().materialLawParams(dummy_).lambda();
    const int veModel = getParam<int>("VE.VEModel");

    const FluidState& fluidStatePlume = this->fluidStatePlume(dummy_, columnRefPressures);
    const Scalar densityW = fluidStatePlume.density(wPhaseIdx);
    const Scalar densityN = fluidStatePlume.density(nPhaseIdx);

    Scalar reconstSaturation = 0.0;

    if (veModel == sharpInterface) //reconstruct phase saturation for sharp interface ve model
    {
        if(height <= minGasPlumeDist)
        {
            reconstSaturation = 1.0;
        }
        else if(height > minGasPlumeDist && height <= gasPlumeDist)
        {
            reconstSaturation = 1.0 - resSatN;
        }
        else if(height > gasPlumeDist)
        {
            reconstSaturation = resSatW;
        }
    }
    else if (veModel == capillaryFringe) //reconstruct phase saturation for capillary fringe model
    {
        if(height <= minGasPlumeDist)
        {
            reconstSaturation = 1.0;
        }
        else if(height > minGasPlumeDist && height <= gasPlumeDist)
        {
            reconstSaturation = 1.0 - resSatN;
        }
        else if(height > gasPlumeDist)
        {
            reconstSaturation = std::pow(((height - gasPlumeDist) * (densityW - densityN)
            * this->gravity().two_norm() + entryP), (-lambda))
            * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
        }
    }

    return reconstSaturation;
}

/*! \brief Calculates integral of difference of wetting saturation over z
 *
 */
const Scalar calculateSatCriterionIntegral(const Scalar lowerBound,
                                           const Scalar upperBound,
                                           const Scalar satW,
                                           const Scalar gasPlumeDist,
                                           const Scalar minGasPlumeDist,
                                           const PhaseVector columnRefPressures) const
{
    int intervalNumber = 10;
    Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

    Scalar satIntegral = 0.0;
    for(int count=0; count<intervalNumber; count++ )
    {
        satIntegral += std::abs((reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures)
        + reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures))/2.0 - satW);
    }
    satIntegral = satIntegral * deltaZ;

    return satIntegral;
}

/*! \brief Calculates integral of difference of relative wetting permeability over z
 *
 */
const Scalar calculateKrwCriterionIntegral(const Scalar lowerBound,
                                     const Scalar upperBound,
                                     const Scalar satW,
                                     const Scalar gasPlumeDist,
                                     const Scalar minGasPlumeDist,
                                     const PhaseVector columnRefPressures) const
{
    int intervalNumber = 10;
    Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

    Scalar krwIntegral = 0.0;
    for(int count=0; count<intervalNumber; count++ )
    {
        Scalar sat1 = reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures);
        Scalar sat2 = reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist, minGasPlumeDist, columnRefPressures);
        Scalar krw1 = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), sat1);
        Scalar krw2 = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), sat2);
        Scalar krw = MaterialLaw::krw(this->spatialParams().materialLawParams(dummy_), satW);
        krwIntegral += std::abs((krw1 + krw2)/2.0 - krw);
    }
    krwIntegral = krwIntegral * deltaZ;

    return krwIntegral;
}

private:
GlobalPosition lowerLeft_;
GlobalPosition upperRight_;

static constexpr Scalar eps_ = 1e-6;
std::string name_;

std::multimap<int, Element> mapColumns_;
std::ofstream outputFile_;
int veModel_;
Element dummy_;
Scalar segTime_;
Scalar viscosityW_;
};
} //end namespace

#endif
