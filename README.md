Summary
=======
This is the DuMuX module containing the code for producing the results
published in "Chapter 5: Multiphysics model for compositional two-phase flow"
of the PhD thesis "Development of efficient multiscale multiphysics models
accounting for reversible flow at various subsurface
energy storage sites" by Beatrix Becker.

Installation
============

You can build the module just like any other DUNE
module.
The easiest way to install this module is to create a new folder and to execute
the file [installBecker20121a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Becker2021a/raw/master/installBecker2021a.sh)
in this folder.
You can copy the following to a terminal:
```bash
mkdir -p Becker2021a && cd Becker2021a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Becker2021a/raw/master/installBecker2021a.sh
chmod +x installBecker2021a.sh && ./installBecker2021a.sh
```
For more detailed informations on installation, have a look at the
[DuMuX installation guide](https://dumux.org/installation/)
or use the [DuMuX handbook](https://dumux.org/docs/).

Applications
============

The applications can be found in the folder `appl` and `test`, particularly in the
following subfolders. Each program is related to a corresponding section in the
publication.

* `energyStorage`: program using a full multidimensional model for calculating reference solutions.
* `decoupled/2p2cve`: program using a full compositional vertical equilibrium model.
* `modelcoupling/2p2cve2p2cfullmono`: program using the multiphysics model coupling compositional vertical equilibrium and compositional full multidimensions.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules as well as
grid managers, have a look at [installBecker2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Becker2021a/raw/master/installBecker2021a.sh).

In addition, the linear solver SuperLU has to be installed.

The module has been tested successfully with GCC 10.2.
The autotools-based DUNE buildsystem has been employed.


Installation with Docker
======================== 

Create a new folder in your favourite location and change into it

```bash
mkdir Becker2021a
cd Becker2021a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Becker2021a/-/raw/master/docker_becker2021a.sh
```
Open the Docker Container
```bash
bash docker_becker2021a.sh open
```
After the script has run successfully, you may build all executables

```bash
cd Becker2021a/build-cmake
make build_tests
```

They are located in the build-cmake folder according to the following paths:

- appl/energyStorage/sequential/storage2p2c
- appl/energyStorage/sequential/2p2c
- test/modelcoupling/2p2cve2p2cfullmono
- test/modelcoupling/2p2cve2p2cfullmono/storagemultidim
- test/decoupled/2p2cve
- test/decoupled/2p2cve/storage2p2cve


It can be executed with an input file, e.g.

```bash
cd appl/energyStorage/sequential/2p2c
./test_dec2p2c test_dec2p2c.input
```
